//
//  UnitViewController.swift
//  Imbanak
//
//  Created by Hovak Davtyan on 1/8/18.
//  Copyright © 2018 alfaSolutions. All rights reserved.
//

import UIKit
import GoogleMaps

class UnitViewController: UIViewController {
    
    var location: Location!
    
    //Main Outlets
    @IBOutlet weak var contactTitleLabel: UILabel!
    @IBOutlet weak var addressLabel: UILabel!
    @IBOutlet weak var imageView: UIImageView!
    @IBOutlet weak var descriptionLabel: UILabel!
    
    //Helper Outlets
    @IBOutlet weak var draggerView: UIView!
    @IBOutlet weak var drawerViewContainer: UIView!
    @IBOutlet weak var imageViewContainer: UIView!
    @IBOutlet weak var headerView: UIView!
    @IBOutlet weak var scrollView: UIScrollView!
    
    //Flags
    var drawerIsShadowConfigured = false
    var imageViewContainerIsShadowConfigured = false
    
    //Constraints
    @IBOutlet weak var scrollViewTopConstraint: NSLayoutConstraint!
    @IBOutlet weak var scrollViewBottomSpaceConstraint: NSLayoutConstraint!
    @IBOutlet weak var tableViewHeightConstraint: NSLayoutConstraint!
    
    //ViewControllers
    var locationViewController: LocationsBackgroundViewController!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        draggerView.layer.cornerRadius = 1.2
        draggerView.layer.masksToBounds = true
        
        if modelIdentifier() == "iPhone10,3" {
            scrollViewBottomSpaceConstraint.constant += 30
        }
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        imageView.image = UIImage(named: location.thumb)
        
        contactTitleLabel.text = location.title
        addressLabel.text = location.address
        
        //Assign TableView Height Constraint
        let phoneCount = CGFloat(location.phoneTitles.count)
        tableViewHeightConstraint.constant = phoneCount * 44.0 + 16
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    override func viewDidLayoutSubviews() {
        super.viewDidLayoutSubviews()
        
        if !drawerIsShadowConfigured {
            configureShadow(view: drawerViewContainer)
            drawerIsShadowConfigured = true
        }
        
        if !imageViewContainerIsShadowConfigured {
            configureShadow(view: imageViewContainer)
            imageViewContainerIsShadowConfigured = true
        }
        
        scrollViewTopConstraint.constant = headerView.frame.height
    }
    
    func configureShadow(view: UIView) {
        //CornerRadius
        view.layer.cornerRadius = 5
        view.layer.masksToBounds = true
        
        
        let shadowViewFrame = CGRect(x: 0, y: 0, width: view.frame.width, height: view.frame.height)
        
        //Shadows
        let shadow1View = UIView(frame: shadowViewFrame)
        let shadow2View = UIView(frame: shadowViewFrame)
        
        shadow1View.layer.shadowColor = UIColor.black.cgColor
        shadow1View.layer.shadowOpacity = 0.16
        shadow1View.layer.shadowOffset = CGSize.zero
        shadow1View.layer.shadowRadius = 0.5
        shadow1View.layer.shadowPath = UIBezierPath(roundedRect: shadowViewFrame, cornerRadius: 5).cgPath
        shadow1View.layer.masksToBounds = false
        view.addSubview(shadow1View)
        view.sendSubview(toBack: shadow1View)
        
        shadow2View.layer.shadowColor = UIColor.black.cgColor
        shadow2View.layer.shadowOpacity = 0.24
        shadow2View.layer.shadowOffset = CGSize(width: 0.0, height: 0.5)
        shadow2View.layer.shadowRadius = 0.5
        shadow2View.layer.shadowPath = UIBezierPath(roundedRect: shadowViewFrame, cornerRadius: 5).cgPath
        shadow2View.layer.masksToBounds = false
        view.addSubview(shadow2View)
        view.sendSubview(toBack: shadow2View)
    }
    
    //MARK: - Actions
    
    @IBAction func closeButton(_ sender: UIButton) {
        locationViewController.bottomController.anchorPanel()
        self.dismiss(animated: true, completion: nil)
    }
    
    @IBAction func reCenter(_ sender: UIButton) {
        //Setup Camera
        let camera = GMSCameraPosition.camera(withLatitude: location.latitude,
                                              longitude: location.longitude,
                                              zoom: 15)
        
        //Move to camera
        locationViewController.mapView.animate(to: camera)
    }
    
    @IBAction func callHotLine(_ sender: UIButton) {
        if let url = URL(string: "tel://\("1-28")"), UIApplication.shared.canOpenURL(url) {
            if #available(iOS 10, *) {
                UIApplication.shared.open(url)
            } else {
                UIApplication.shared.openURL(url)
            }
        }
    }
    
    func modelIdentifier() -> String {
        if let simulatorModelIdentifier = ProcessInfo().environment["SIMULATOR_MODEL_IDENTIFIER"] { return simulatorModelIdentifier }
        var sysinfo = utsname()
        uname(&sysinfo) // ignore return value
        return String(bytes: Data(bytes: &sysinfo.machine, count: Int(_SYS_NAMELEN)), encoding: .ascii)!.trimmingCharacters(in: .controlCharacters)
    }

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}

extension UnitViewController: UITableViewDelegate, UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return location.phoneTitles.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cell = tableView.dequeueReusableCell(withIdentifier: "unitContactCell")!
        
        if let contactCell = cell as? LocationUnitContactTableViewCell {
            contactCell.phoneLabel.text = location.phoneTitles[indexPath.row]
        }
        
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        print("didSelectRow")
        
        // WARNING: - Examine Call Functionality
        if let url = URL(string: "tel://\(location.phoneNumbers[indexPath.row])"), UIApplication.shared.canOpenURL(url) {
            if #available(iOS 10, *) {
                UIApplication.shared.open(url)
            } else {
                UIApplication.shared.openURL(url)
            }
        }
    }
}

extension UnitViewController: UIScrollViewDelegate {
    func scrollViewDidScroll(_ scrollView: UIScrollView) {
        if scrollView.contentOffset.y <= 0 {
            if scrollView.isScrollEnabled {
                scrollView.isScrollEnabled = false
                print("scrollingDisabled")
            }
        } else {
            if !scrollView.isScrollEnabled {
                scrollView.isScrollEnabled = true
                print("scrollingEnabled")
            }
        }
    }
}
