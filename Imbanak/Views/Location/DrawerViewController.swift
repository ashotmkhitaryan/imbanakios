//
//  DrawerViewController.swift
//  Imbanak
//
//  Created by Hovak Davtyan on 1/8/18.
//  Copyright © 2018 alfaSolutions. All rights reserved.
//

import UIKit
import CTSlidingUpPanel
import GoogleMaps

class DrawerViewController: UIViewController {
    
    let defaults = UserDefaults.standard
    
    //Map Properties
    var locations = [Location]()
    var history = [Location]()
    var searchResults = [Location]()
    var searchKeywords = [String]()
    var locationContact: Location!

    //View Outlets
    @IBOutlet weak var draggerView: UIView!
    @IBOutlet weak var drawerViewContainer: UIView!
    @IBOutlet weak var searchBar: UISearchBar!
    @IBOutlet weak var locationTableView: UITableView!
    @IBOutlet weak var locationTableViewFooter: UIView!
    
    //Flags
    var drawerIsShadowConfigured = false
    var isSearchActive = false {
        didSet {
            if isSearchActive {
                locationTableViewFooter.frame.size.height = 216
                locationTableView.scrollIndicatorInsets.bottom = 216
            } else {
                locationTableViewFooter.frame.size.height = 100
                locationTableView.scrollIndicatorInsets.bottom = 0
            }
        }
    }
    
    //ViewControllers
    var locationViewController: LocationsBackgroundViewController!
    var unitViewController: UnitViewController?

    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        //Load FullLocations
        if let filePath = Bundle.main.path(forResource: "AllAddresses", ofType: "plist") {
            if let locationsPlist = NSArray(contentsOfFile: filePath) as? [[String:Any]] {
                for location in locationsPlist {
                    
                    guard let thumb = location["thumb"] as? String else { return }
                    
                    guard let typeString = location["type"] as? String else { print("failType"); return }
                    
                    var type: LocationType {
                        switch typeString {
                        case "milOffice": return .milOffice
                        case "hospital": return .hospital
                        case "university": return .university
                        default: return .general
                        }
                    }

                    guard let title = location["title"] as? String else { return }
                    
                    guard let latitude = location["latitude"] as? Double else { return }
                    guard let longitude = location["longitude"] as? Double else { return }
                    
                    guard let address = location["address"] as? String else { return }
                    
                    guard let phones = location["phones"] as? [String:String] else { return }
                    let phoneTitles = Array(phones.keys)
                    let phoneNumbers = Array(phones.values)
                    
                    let locationUnit = Location(thumb: thumb, type: type, title: title, longitude: longitude, latitude: latitude, address: address, phoneTitles: phoneTitles, phoneNumbers: phoneNumbers)
                    
                    locations.append(locationUnit)
                }
            }
        }
        
        
        //Get history
        if let historyData = defaults.data(forKey: "mapHistory") {
            if let decodedData = try? JSONDecoder().decode([Location].self, from: historyData) {
            history = decodedData
            }
        } else {
            //Create row History with handpicked entries
            let rowHistory = [
                locations[57],
                locations[58],
                locations[54],
                locations[45],
                locations[52],
                locations[53],
                locations[55],
                locations[47],
                locations[60],
                locations[0]
            ]
            
            //Assign to curent history
            history = rowHistory
            
            //Save to userDefaults
            let encodedData = try? JSONEncoder().encode(rowHistory)
            defaults.set(encodedData, forKey: "mapHistory")
            defaults.synchronize()
        }
        
        self.searchBar.setSearchFieldBackground(color: UIColor.piSteel.withAlphaComponent(0.12), height: 36, cornerRadius: 10, for: .normal)
        self.searchBar.setValue("Չեղարկել", forKey:"_cancelButtonText")
        self.searchBar.delegate = self
        
        draggerView.layer.cornerRadius = 1.2
        draggerView.layer.masksToBounds = true
        
        if let parentVC = parent as? UINavigationController {
            locationViewController = parentVC.parent as? LocationsBackgroundViewController
        }
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    override func viewDidLayoutSubviews() {
        super.viewDidLayoutSubviews()
        
        if !drawerIsShadowConfigured {
            configureDrawerShadow()
        }
    }
    
    func configureDrawerShadow() {
        //CornerRadius
        drawerViewContainer.layer.cornerRadius = 5
        drawerViewContainer.layer.masksToBounds = true
        
        
        let shadowViewFrame = CGRect(x: 0, y: 0, width: drawerViewContainer.frame.width, height: drawerViewContainer.frame.height)
        
        //Shadows
        let shadow1View = UIView(frame: shadowViewFrame)
        let shadow2View = UIView(frame: shadowViewFrame)
        
        shadow1View.layer.shadowColor = UIColor.black.cgColor
        shadow1View.layer.shadowOpacity = 0.16
        shadow1View.layer.shadowOffset = CGSize.zero
        shadow1View.layer.shadowRadius = 0.5
        shadow1View.layer.shadowPath = UIBezierPath(roundedRect: shadowViewFrame, cornerRadius: 5).cgPath
        shadow1View.layer.masksToBounds = false
        drawerViewContainer.addSubview(shadow1View)
        drawerViewContainer.sendSubview(toBack: shadow1View)
        
        shadow2View.layer.shadowColor = UIColor.black.cgColor
        shadow2View.layer.shadowOpacity = 0.24
        shadow2View.layer.shadowOffset = CGSize(width: 0.0, height: 0.5)
        shadow2View.layer.shadowRadius = 0.5
        shadow2View.layer.shadowPath = UIBezierPath(roundedRect: shadowViewFrame, cornerRadius: 5).cgPath
        shadow2View.layer.masksToBounds = false
        drawerViewContainer.addSubview(shadow2View)
        drawerViewContainer.sendSubview(toBack: shadow2View)
        
        drawerIsShadowConfigured = true
    }
    


    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        
        print("willSegue")

        if segue.identifier == "UnitSegue" {
            if let destination = segue.destination as? UnitViewController {
                unitViewController = destination
                destination.locationViewController = locationViewController
                destination.location = locationContact
            }
        }
        
    }


}

extension DrawerViewController: UITableViewDelegate, UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        
        return isSearchActive ? searchResults.count : history.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cell = tableView.dequeueReusableCell(withIdentifier: "locationCell")!
        let dataSource = isSearchActive ? searchResults : history
        
        if let locationCell = cell as? LocationTableViewCell {
            
            //Configuring and assigning title
            let titleText = dataSource[indexPath.row].title
            let notHighlightedFont = [NSAttributedStringKey.font : UIFont(name: "WeblySleekUISemilight", size: 17)!]
            let highlightedFont = [NSAttributedStringKey.font : UIFont(name: "WeblySleekUISemibold", size: 17)!]
            
            if isSearchActive {
                let attributedTitleText = NSMutableAttributedString(string: titleText, attributes: notHighlightedFont)
                for keyword in searchKeywords {
                let range = (titleText as NSString).range(of: keyword, options: .caseInsensitive)
                attributedTitleText.addAttributes(highlightedFont, range: range)
                locationCell.titleLabel.attributedText = attributedTitleText
                }
            } else {
                let attributedTitleText = NSAttributedString(string: titleText, attributes: highlightedFont)
                locationCell.titleLabel.attributedText = attributedTitleText
            }
            
            locationCell.addressLabel.text = dataSource[indexPath.row].address
            
            switch dataSource[indexPath.row].type {
            case .milOffice: locationCell.markerImageView.image = UIImage(named: "milOfficeIcon")
            case .hospital : locationCell.markerImageView.image = UIImage(named: "hospitalIcon")
            case .university: locationCell.markerImageView.image = UIImage(named: "universityIcon")
            case .general: locationCell.markerImageView.image = UIImage(named: "generalIcon")
            }
        }
        
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        print("didSelect")
        
        locationViewController.mapView.clear()
        
        let dataSource = isSearchActive && !searchKeywords.isEmpty ? searchResults : history
        
        let location = dataSource[indexPath.row]
        locationContact = location //for Segue
        
        //Setup Camera
        let camera = GMSCameraPosition.camera(withLatitude: location.latitude,
                                              longitude: location.longitude,
                                              zoom: 15)
        
        //Setup Marker
        let marker = GMSMarker()
        marker.position = CLLocationCoordinate2D(latitude: location.latitude,
                                                 longitude: location.longitude)
        
        marker.title = location.title
        marker.snippet = location.address
        
        switch location.type {
        case .general: marker.icon = UIImage(named: "generalMarker")
        case .milOffice: marker.icon = UIImage(named: "milOfficeMarker")
        case .hospital: marker.icon = UIImage(named: "hospitalMarker")
        case .university : marker.icon = UIImage(named: "universityMarker")
        }

        marker.map = locationViewController.mapView
        
        locationViewController.mapView.animate(to: camera)
        locationViewController.bottomController.anchorPanel()
        
        //Adding to History
        if history.contains(location) {
            if let index = history.index(of: location) {
                history.remove(at: index)
            }
        }
        
        history.insert(location, at: 0)
        locationTableView.reloadData()
        
        let encodedData = try? JSONEncoder().encode(history)
        defaults.set(encodedData, forKey: "mapHistory")
        defaults.synchronize()
        
        
        performSegue(withIdentifier: "UnitSegue", sender: self)
    }
    
    func scrollViewDidScroll(_ scrollView: UIScrollView) {
        if (searchBar.text?.isEmpty)! {
            searchBar.resignFirstResponder()
        }
        
        if scrollView.contentOffset.y <= 0 {
            if scrollView.isScrollEnabled {
                scrollView.isScrollEnabled = false
                print("scrollingDisabled")
            }
        } else {
            if !scrollView.isScrollEnabled {
                scrollView.isScrollEnabled = true
                print("scrollingEnabled")
            }
        }
    }
}

extension DrawerViewController: UISearchBarDelegate {
    
    func searchBarTextDidBeginEditing(_ searchBar: UISearchBar) {
        isSearchActive = true
        searchBar.setShowsCancelButton(true, animated: true)
        locationViewController.bottomController.expandPanel()
    }
    
    func searchBarTextDidEndEditing(_ searchBar: UISearchBar) {
        isSearchActive = false
        searchBar.text = ""
        searchBar.setShowsCancelButton(false, animated: true)
        locationTableView.reloadData()
    }
    
    func searchBarCancelButtonClicked(_ searchBar: UISearchBar) {
        isSearchActive = false
        searchResults = [Location]()
        searchBar.resignFirstResponder()
        searchBar.text = ""
        searchBar.setShowsCancelButton(false, animated: true)
        locationViewController.bottomController.anchorPanel()
        locationTableView.reloadData()
    }
    
    func searchBarSearchButtonClicked(_ searchBar: UISearchBar) {
        isSearchActive = false
        searchResults = [Location]()
        searchBar.resignFirstResponder()
        searchBar.text = ""
        searchBar.setShowsCancelButton(false, animated: true)
        locationViewController.bottomController.anchorPanel()
        locationTableView.reloadData()
    }
    
    func searchBar(_ searchBar: UISearchBar, textDidChange searchText: String) {
        if searchText.isEmpty {
            isSearchActive = false
            locationTableView.reloadData()
        } else {
            isSearchActive = true
            searchResults = [Location]()
            searchKeywords = [String]()
            for version in KeyboardMapper.map(engText: searchText) {
                searchKeywords.append(version)
                let results = locations.filter { (location) -> Bool in
                    let title = location.title
                    let range = title.range(of: version, options: .caseInsensitive)
                    if let _ = range {
                        return true
                    } else {
                        return false
                    }
                }
                if !results.isEmpty {
                    searchResults.append(contentsOf: results)
                }
            }
            locationTableView.reloadData()
        }
    }
}
