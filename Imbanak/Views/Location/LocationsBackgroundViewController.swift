//
//  LocationsBackgroundViewController.swift
//  Imbanak
//
//  Created by Hovak Davtyan on 1/6/18.
//  Copyright © 2018 alfaSolutions. All rights reserved.
//

import UIKit
import GoogleMaps
import CTSlidingUpPanel

class LocationsBackgroundViewController: UIViewController {
    
    //Maps
    var camera: GMSCameraPosition!
    @IBOutlet weak var mapView: GMSMapView!
    
    //Drawer
    @IBOutlet weak var bottomViewContainer: UIView!
    var bottomViewController: DrawerViewController!
    var bottomController: CTBottomSlideController!
    
    @IBOutlet weak var blurredStatusBarHeightConstraint: NSLayoutConstraint!
    
    override var shouldAutorotate: Bool {
        return false
    }
    
    override var supportedInterfaceOrientations: UIInterfaceOrientationMask {
        return .portrait
    }
    
    override var preferredInterfaceOrientationForPresentation: UIInterfaceOrientation {
        return .portrait
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        //Set statusBar bluerd view height
        blurredStatusBarHeightConstraint.constant = UIApplication.shared.statusBarFrame.height
        
        
        //Configure initial map
        camera = GMSCameraPosition.camera(withLatitude: 40.209405, longitude: 44.57826, zoom: 15)
        mapView.settings.compassButton = true
        mapView.camera = camera
        
        //Add Marker
        let marker = GMSMarker()
        marker.position = CLLocationCoordinate2D(latitude: 40.209405, longitude: 44.57826)
        marker.title = "ՀՀ ՊՆ"
        marker.icon = UIImage(named: "generalMarker")
        marker.snippet = "Երևան, Բագրևանդի 5"
        marker.map = mapView
        
        //Configure Bottom Controller
        bottomController = CTBottomSlideController(parent: view, bottomView: bottomViewContainer, tabController: self.tabBarController, navController: nil, visibleHeight: 64)
        bottomController.setAnchorPoint(anchor: 0.6)
        bottomController.delegate = self
    }
    

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        
        bottomController.anchorPanel()
    }
    
    // MARK: - Actions
    @IBAction func toogleSattelite(_ sender: UIButton) {
        if mapView.mapType == .normal {
            mapView.mapType = .satellite
            sender.setImage(UIImage(named: "satelliteOff"), for: .normal)
        } else {
            mapView.mapType = .normal
            sender.setImage(UIImage(named: "satelliteOn"), for: .normal)
        }
    }
    


    // MARK: - Navigation
    
    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == "embedBottomView" {
            if let destination = segue.destination as? UINavigationController {
                destination.isNavigationBarHidden = true
                bottomViewController = destination.visibleViewController as? DrawerViewController
            }
        }
    }
}

extension LocationsBackgroundViewController: CTBottomSlideDelegate {
    func didPanelCollapse()
    {
        if (bottomViewController.searchBar?.isFirstResponder)! {
            bottomViewController.searchBar?.resignFirstResponder()
        }
        
        bottomViewController.locationTableView.isScrollEnabled = false
        bottomViewController.unitViewController?.scrollView.isScrollEnabled = false
        
        let statusBarHeight = UIApplication.shared.statusBarFrame.height
        let mapInsets = UIEdgeInsets(top: statusBarHeight , left: 0.0, bottom: 64, right: 0.0)
        mapView.padding = mapInsets
        print("Collapsed");
    }
    func didPanelExpand(){
        bottomViewController.locationTableView.isScrollEnabled = true
        bottomViewController.unitViewController?.scrollView.isScrollEnabled = true
        print("Expanded")
    }
    func didPanelAnchor(){
        print("didAnchor")
        if (bottomViewController.searchBar?.isFirstResponder)! {
            bottomViewController.searchBar?.resignFirstResponder()
        }
        
        bottomViewController.locationTableView.isScrollEnabled = false
        bottomViewController.unitViewController?.scrollView.isScrollEnabled = false
        
        let statusBarHeight = UIApplication.shared.statusBarFrame.height
        let tabBarHeight = (tabBarController?.tabBar.frame.height)!
        let mapInsetBottom = UIScreen.main.bounds.height * 0.6
            - tabBarHeight
            - statusBarHeight + statusBarHeight * 0.4
        let mapInsets = UIEdgeInsets(top: statusBarHeight, left: 0.0, bottom: mapInsetBottom, right: 0.0)
        mapView.padding = mapInsets
    }
    
    func didPanelMove(panelOffset: CGFloat)
    {
        print(panelOffset);
    }
}
