//
//  LocationUnitContactTableViewCell.swift
//  Imbanak
//
//  Created by Hovak Davtyan on 1/9/18.
//  Copyright © 2018 alfaSolutions. All rights reserved.
//

import UIKit

class LocationUnitContactTableViewCell: UITableViewCell {
    @IBOutlet weak var phoneLabel: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
