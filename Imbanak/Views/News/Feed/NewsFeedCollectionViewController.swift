//
//  NewsFeedCollectionViewController.swift
//  Imbanak
//
//  Created by Hovak Davtyan on 12/4/17.
//  Copyright © 2017 alfaSolutions. All rights reserved.
//

import UIKit
import Kingfisher

class NewsFeedCollectionViewController: UICollectionViewController {
    
    private let reuseIdentifier = "NewsCell"
    private let refreshControl = UIRefreshControl()
    private let sectionInsets = UIEdgeInsets(top: 16, left: 16, bottom: 16, right: 16)
    private let itemsPerRow: CGFloat = 1
    private var isShowingLargeTitle: Bool = false
    var searchController: UISearchController!
    
    private var newsFeed = [ArticleResponse.Article]() {
        didSet {
            DispatchQueue.main.async {
                if self.newsFeed.count != 0 {
                    self.activityIndicator.stopAnimating()
                    self.refreshControl.endRefreshing()
                }
                self.collectionView?.reloadData()
            }
        }
    }
    
    
    //Flags
    var newsFeedTotal = 0
    var newsFeedIsFull = false
    
    @IBOutlet weak var searchBarButtonItem: UIBarButtonItem!
    @IBOutlet weak var activityIndicator: UIActivityIndicatorView!
    
    //Orientation Properties
    override var shouldAutorotate: Bool {
        return false
    }
    
    override var supportedInterfaceOrientations: UIInterfaceOrientationMask {
        return .portrait
    }
    
    override var preferredInterfaceOrientationForPresentation: UIInterfaceOrientation {
        return .portrait
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        print("ViewDidLoad")
        
        //Add Refresh Control
        collectionView?.refreshControl = refreshControl
        refreshControl.addTarget(self, action: #selector(refresh), for: .valueChanged)
        
        let storyboard = UIStoryboard(name: "Main", bundle: nil)
        let searchTableViewController = storyboard.instantiateViewController(withIdentifier: "searchResults") as! SearchTableViewController
        searchController = UISearchController(searchResultsController: searchTableViewController)
        searchController.dimsBackgroundDuringPresentation = false
        searchController.delegate = self
        searchController.searchBar.tintColor = UIColor.piDarkBlueGrey
        searchController.searchBar.placeholder = "Որոնում"
        searchController.searchBar.setValue("Չեղարկել", forKey:"_cancelButtonText")
        searchController.searchResultsUpdater = searchTableViewController
        
        navigationItem.searchController = searchController
        definesPresentationContext = true
        
        self.title = "Լրահոս"
    
        navigationController?.navigationBar.prefersLargeTitles = true
        navigationController?.navigationItem.hidesSearchBarWhenScrolling = false
        navigationController?.navigationBar.tintColor = UIColor.piDarkBlueGrey
        activityIndicator.startAnimating()
            parseNews(count: 10)
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        print("viewDidAppear")
    }
    
    @IBAction func searchButton(_ sender: UIBarButtonItem) {
        navigationItem.searchController?.isActive = true
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        switchNavigationBarLargeTitle()
    }
    
    //MARK: - Navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        switch segue.identifier! {
        //MainSegues
        case "GenericSegue":
            guard let destination = segue.destination as? NewsArticleTableViewController, let article = sender as? ArticleResponse.Article else { return }
            destination.feedArticle = article
        case "VideoSegue":
            guard let destination = segue.destination as? NewsYoutubePlayerViewController, let article = sender as? ArticleResponse.Article else { return }
            destination.feedArticle = article
        //SearchSegues
        case "SearchNewsSegue":
            guard let destination = segue.destination as? NewsArticleTableViewController, let article = sender as? ArticleResponse.Article else { return }
            destination.feedArticle = article
        case "SearchMilServicesSegue":
            guard let destination = segue.destination as? MilServiceArticleTableViewController, let article = sender as? ArticleResponse.Article else { return }
            destination.feedArticle = article
        case "SearchAnnouncementsSegue":
            guard let destination = segue.destination as? AnnouncementsArticleTableViewController, let article = sender as? ArticleResponse.Article else { return }
            destination.feedArticle = article
        case "SearchVideoSegue":
            guard let destination = segue.destination as? NewsYoutubePlayerViewController, let article = sender as? ArticleResponse.Article else { return }
            destination.feedArticle = article
        default: return
        }
    }
    
}

// MARK: UICollectionViewDataSource and Delegate

extension NewsFeedCollectionViewController {
    
    //Data Source
    override func numberOfSections(in collectionView: UICollectionView) -> Int {
        // #warning Incomplete implementation, return the number of sections
        return 1
    }
    
    
    override func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        guard (newsFeed.count != 0) else { return 0 }
        return newsFeed.count
    }
    
    override func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: reuseIdentifier, for: indexPath)
        
        if let newsFeedCell = cell as? NewsFeedCollectionViewCell {
            let article = newsFeed[indexPath.row]
                newsFeedCell.setArticle(article)
        }
        return cell
    }
    
    //Delegate
    override func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        var segueIdentifier = ""
        let article = newsFeed[indexPath.row]
        switch article.thumb {
        case .generic: segueIdentifier = "GenericSegue"
        case .video: segueIdentifier = "VideoSegue"
        case .gallery: segueIdentifier = "GenericSegue"
        }
        performSegue(withIdentifier: segueIdentifier, sender: article)
    }
    
    
    
    override func collectionView(_ collectionView: UICollectionView, willDisplay cell: UICollectionViewCell, forItemAt indexPath: IndexPath) {
        if indexPath.row == (newsFeed.count) - 1 {
            print("LastCell")
            
            print("newsFeedIsFull\(newsFeedIsFull)")
            print("newsFeedCount\(newsFeed.count)")
            if !newsFeedIsFull && (newsFeed.count >= 10) {
                print("willParse")
            parseNews(count: 10)
            }
        }
    }
}

// MARK: - UICollectionViewFlawLayoutDelegate

extension NewsFeedCollectionViewController: UICollectionViewDelegateFlowLayout {
    
    func collectionView(_ collectionView: UICollectionView,
                        layout collectionViewLayout: UICollectionViewLayout,
                        sizeForItemAt indexPath: IndexPath) -> CGSize {
        
        let ratio: CGFloat = 265 / 343
        let widthPerItem = view.frame.size.width - sectionInsets.left * 2
        
        //Calculate Label Height for item at IndexPath
        var labelSize: CGFloat = 0.0
        let label = UILabel(frame: CGRect(x: 0, y: 0, width: widthPerItem, height: 27))
        let font = UIFont(name: "WeblySleekUISemibold", size: 20)
        label.font = font
        label.numberOfLines = 0
        label.textAlignment = .left
        label.lineBreakMode = .byWordWrapping
        label.text = newsFeed[indexPath.row].title
        label.sizeToFit()
        labelSize = label.frame.size.height
        
        let heightPerItem = ratio * widthPerItem - 27 + labelSize
        
        return CGSize(width: widthPerItem, height: heightPerItem)
    }
    
    func collectionView(_ collectionView: UICollectionView,
                        layout collectionViewLayout: UICollectionViewLayout,
                        insetForSectionAt section: Int) -> UIEdgeInsets {
        return sectionInsets
    }
    
    func collectionView(_ collectionView: UICollectionView,
                        layout collectionViewLayout: UICollectionViewLayout,
                        minimumLineSpacingForSectionAt section: Int) -> CGFloat {
        return 17.0
    }
    
}

// MARK: - UIScrollViewDelegate

//extension NewsFeedCollectionViewController {
//    override func scrollViewDidScroll(_ scrollView: UIScrollView) {
//        switchNavigationBarLargeTitle()
//    }
//}


// MARK: - UISearchControllerDelegate

extension NewsFeedCollectionViewController: UISearchControllerDelegate {
    
    func didPresentSearchController(_ searchController: UISearchController) {
        self.title = "Որոնում"
 
        navigationController?.navigationBar.barTintColor = UIColor.piWhite
        isShowingLargeTitle = false

        UIView.beginAnimations(nil, context: nil)
        UIView.setAnimationDuration(0.3)
        self.navigationItem.searchController?.searchBar.alpha = 1.0
        self.navigationItem.searchController?.searchBar.setSearchFieldBackground(color: UIColor.piSteel.withAlphaComponent(0.12), height: 36, cornerRadius: 6, for: .normal)
        UIView.commitAnimations()

        self.navigationItem.searchController?.searchBar.becomeFirstResponder()
    }
    
    func willDismissSearchController(_ searchController: UISearchController) {
        self.title = "Լրահոս"
    }

}

// MARK: - NewsFeedCollectionViewController Extension

extension NewsFeedCollectionViewController {
    
    func switchNavigationBarLargeTitle() {
        if (navigationController?.navigationBar.frame.height)! < CGFloat(60) {
            isShowingLargeTitle = false
            
            navigationController?.navigationBar.barTintColor = UIColor.piWhite
            self.searchBarButtonItem.image = UIImage(named: "searchIcon")
            navigationController?.navigationBar.setValue(false, forKey: "hidesShadow")
            self.navigationItem.searchController?.searchBar.setSearchFieldBackground(color: UIColor.piSteel.withAlphaComponent(0.12), height: 36, cornerRadius: 6, for: .normal)
        } else if (navigationController?.navigationBar.frame.height)! > CGFloat(60) {
            isShowingLargeTitle = true
            
            navigationController?.navigationBar.barTintColor = UIColor.piWhiteTwo
            searchBarButtonItem.image = UIImage()
            navigationController?.navigationBar.setValue(true, forKey: "hidesShadow")
            
            //Configure Search
            self.navigationItem.searchController?.searchBar.setSearchFieldBackground(color: UIColor.piWhiteThree, height: 48, cornerRadius: 6, for: .normal)
        }
    }
    
    @objc func refresh() {
        print("willRefresh")
        parseNews(count: 10)
    }
    
    func parseNews(count: Int) {
        //Get Articles
        let start = refreshControl.isRefreshing ? 0 : newsFeed.count
        var limit = count
        print(newsFeedTotal)
        if newsFeedTotal - start < 10 && newsFeedTotal - start > 0 {
            newsFeedIsFull = true
            limit = newsFeedTotal - start
            print("newsFeedTotal: \(newsFeedTotal)")
        }
        
        guard let articlesURL = URL(string: "http://modpub.ticcotaa.ru/api/v1/articles/newsfeed/?start=\(start)&limit=\(limit)") else {
            print("wrongURL")
            return
        }
        
        let request = URLRequest.init(url: articlesURL)
        let session = URLSession.shared
        let task = session.dataTask(with: request, completionHandler: {
            (data, response, error) in
            guard error == nil else {
                //Handle error
                print(error ?? "Unknown Error Accured")
                return
            }
            
            guard let responseData = data else {
                print("Error: did not receive data")
                return
            }
            
            do {
                if let articleResultsJSON = try JSONSerialization.jsonObject(with: responseData, options: []) as? [String: Any],
                    let newsFeed = ArticleResponse(json: articleResultsJSON) {
                    DispatchQueue.main.async {
                        if self.refreshControl.isRefreshing {
                            self.newsFeed = newsFeed.articles
                        } else {
                            print("Will Append \(newsFeed.articles.count) news")
                            self.newsFeed.append(contentsOf: newsFeed.articles)
                        }
                        self.newsFeedTotal = newsFeed.total
                        print(self.newsFeedTotal)
                    }
                    
                    print(articlesURL.absoluteString)
                    // created a NewsFeed object
                } else {
                    // couldn't create a NewsFeed object from the JSON
                    print("Couldn't create a todo object from the JSON")
                }
            } catch {
                // error trying to convert the data to JSON using JSONSerialization.jsonObject
                print("cannot Convert data through serialization")
                return
            }
        })
        
        task.resume()
        
    }
}
