//
//  NewsFeedTableViewController.swift
//  Imbanak
//
//  Created by Hovak Davtyan on 2/5/18.
//  Copyright © 2018 alfaSolutions. All rights reserved.
//

import UIKit
import Kingfisher

class NewsFeedTableViewController: UITableViewController {
    var networkController: NetworkController!
    var searchController: UISearchController!
    
    private var headlines = [Headline]()
    
    //Flags
    private var newsFeedTotal = 0
    private var newsFeedIsFull = false
    
    @IBOutlet weak var searchBarButtonItem: UIBarButtonItem!
    @IBOutlet weak var customLoader: IBActivityIndicator!
    var refreshIndicator = IBActivityIndicator()
    
    //Orientation Properties
    override var shouldAutorotate: Bool {
        return false
    }
    
    override var supportedInterfaceOrientations: UIInterfaceOrientationMask {
        return .portrait
    }
    
    override var preferredInterfaceOrientationForPresentation: UIInterfaceOrientation {
        return .portrait
    }

    override func viewDidLoad() {
        super.viewDidLoad()
        
        //TableView Cell Dimensions
        tableView.rowHeight = UITableViewAutomaticDimension
        tableView.estimatedRowHeight = 280
        
        //Add Custom Refresh Control Loader
        tableView?.refreshControl = UIRefreshControl()
        tableView.refreshControl?.tintColor = UIColor.clear
        refreshControl?.addTarget(self, action: #selector(refresh(_:)), for: .valueChanged)
        
        tableView.refreshControl?.addSubview(refreshIndicator)
        refreshIndicator.translatesAutoresizingMaskIntoConstraints = false
        NSLayoutConstraint.activate([
            refreshIndicator.centerXAnchor.constraint(equalTo: (refreshControl?.centerXAnchor)!),
            refreshIndicator.centerYAnchor.constraint(equalTo: (refreshControl?.centerYAnchor)!),
            refreshIndicator.heightAnchor.constraint(equalToConstant: 30),
            refreshIndicator.widthAnchor.constraint(equalToConstant: 30)
            ])
        
        //Search
        let storyboard = UIStoryboard(name: "Main", bundle: nil)
        let searchTableViewController = storyboard.instantiateViewController(withIdentifier: "searchResults") as! SearchTableViewController
        searchTableViewController.networkController = networkController
        searchController = UISearchController(searchResultsController: searchTableViewController)
        searchController.dimsBackgroundDuringPresentation = false
        searchController.delegate = self
        searchController.searchBar.tintColor = UIColor.piDarkBlueGrey
        searchController.searchBar.placeholder = "Որոնում"
        searchController.searchBar.setValue("Չեղարկել", forKey:"_cancelButtonText")
        searchController.searchResultsUpdater = searchTableViewController
        
        navigationItem.searchController = searchController
        definesPresentationContext = true
        
        self.title = "Լրահոս"
        navigationController?.navigationItem.hidesSearchBarWhenScrolling = false
        navigationController?.navigationBar.tintColor = UIColor.piDarkBlueGrey
        
        //Get News
        networkController.loadHeadlines(from: 0, to: 10, ofType: .newsfeed) { [weak self]
            headlines in
            guard let headlines = headlines else { return }
            self?.headlines = headlines
            self?.tableView.reloadData()
        }
    }

    @IBAction func searchButton(_ sender: UIBarButtonItem) {
        navigationItem.searchController?.isActive = true
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        tableView.reloadData()
        if let navigationController = self.navigationController as? IBNavigationController {
            navigationController.switchStyle()
        }
    }
    
    //MARK: - Navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        switch segue.identifier! {
            
        //MainSegues
        case "GenericSegue":
            guard let destination = segue.destination as? NewsArticleTableViewController, let headline = sender as? Headline else { return }
            destination.headline = headline
            destination.networkController = networkController
        case "VideoSegue":
            guard let destination = segue.destination as? NewsYoutubePlayerViewController, let headline = sender as? Headline else { return }
            destination.headline = headline
            destination.networkController = networkController
            
        //SearchSegues
        case "SearchNewsSegue":
            guard let destination = segue.destination as? NewsArticleTableViewController, let headline = sender as? Headline else { return }
            destination.headline = headline
            destination.networkController = networkController
        case "SearchVideoSegue":
            guard let destination = segue.destination as? NewsYoutubePlayerViewController, let headline = sender as? Headline else { return }
            destination.headline = headline
            destination.networkController = networkController
        default: return
        }
    }
}

// MARK: - Table view data source
extension NewsFeedTableViewController {
    
    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        guard headlines.count != 0 else { return 10 }
        return headlines.count
    }
    
    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        if headlines.isEmpty {
            let cell = tableView.dequeueReusableCell(withIdentifier: "SkeletonCell", for: indexPath)
            cell.startShimmering(withIntensity: 0.6)
            return cell
        } else {
            let cell = tableView.dequeueReusableCell(withIdentifier: "NewsCell", for: indexPath)
            
            if let newsFeedCell = cell as? NewsFeedTableViewCell {
                let headline = headlines[indexPath.row]
                newsFeedCell.headline = headline
            }
            
            return cell
        }
    }
}

// MARK: - Table view Delegate
extension NewsFeedTableViewController {
    override func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        if !headlines.isEmpty {
            let article = headlines[indexPath.row]
            
            switch article.thumb {
            case .generic: performSegue(withIdentifier: "GenericSegue", sender: article)
            case .video: performSegue(withIdentifier: "VideoSegue", sender: article)
            case .gallery: performSegue(withIdentifier: "GenericSegue", sender: article)
            }
        }
    }
    
    override func tableView(_ tableView: UITableView, willDisplay cell: UITableViewCell, forRowAt indexPath: IndexPath) {
        if !headlines.isEmpty {
            if indexPath.row == (headlines.count) - 1 {
                if !newsFeedIsFull && (headlines.count >= 10) {
                    self.customLoader.isHidden = false
                    self.customLoader.startAnimating()
                    networkController.currentTask?.cancel()
                    networkController.loadHeadlines(from: headlines.count, to: 10, ofType: .newsfeed) {
                        newHeadlines in
                        guard let newHeadlines = newHeadlines else { return }
                        self.headlines += newHeadlines
                        self.tableView.beginUpdates()
                        let newIndicesFloor = self.headlines.count - newHeadlines.count
                        var indexPaths = [IndexPath]()
                        for index in self.headlines.indices where index >= newIndicesFloor  {
                            indexPaths.append(IndexPath(row: index, section: 0))
                        }
                        self.tableView.insertRows(at: indexPaths, with: .automatic)
                        self.tableView.endUpdates()
                    }
                }
            }
        }
    }
}

// MARK: - UIScrollViewDelegate
extension NewsFeedTableViewController {
    
    override func scrollViewDidScroll(_ scrollView: UIScrollView) {
        if !headlines.isEmpty {
            if !(tableView.refreshControl?.isHidden)! {
                if (tableView.refreshControl?.isRefreshing)! {
                    refreshIndicator.startAnimating()
                } else {
                    refreshIndicator.animateFor(offset: scrollView.contentOffset.y)
                }
            } else {
                refreshIndicator.stopAnimating()
            }
        }
        
        if let navigationController = self.navigationController as? IBNavigationController {
            navigationController.switchStyle()
            searchBarButtonItem.image = navigationController.isShowingLargeTitle ? UIImage() : UIImage(named: "searchIcon")
        }
    }
}

// MARK: - UISearchControllerDelegate
extension NewsFeedTableViewController: UISearchControllerDelegate {
    func didPresentSearchController(_ searchController: UISearchController) {
        self.title = "Որոնում"
        
        navigationController?.navigationBar.barTintColor = UIColor.piWhite
        
        UIView.beginAnimations(nil, context: nil)
        UIView.setAnimationDuration(0.3)
        self.navigationItem.searchController?.searchBar.alpha = 1.0
        self.navigationItem.searchController?.searchBar.setSearchFieldBackground(color: UIColor.piSteel.withAlphaComponent(0.12), height: 36, cornerRadius: 6, for: .normal)
        UIView.commitAnimations()
        
        self.navigationItem.searchController?.searchBar.becomeFirstResponder()
    }
    
    func willDismissSearchController(_ searchController: UISearchController) {
        self.title = "Լրահոս"
    }
}

// MARK: - Methods
extension NewsFeedTableViewController {
    
    @objc func refresh(_ sender: UIRefreshControl) {
        if !headlines.isEmpty {
            networkController.currentTask?.cancel()
            networkController.loadHeadlines(from: 0, to: 10, ofType: .newsfeed) {
                headlines in
                guard let headlines = headlines else { return }
                self.headlines = headlines
                self.tableView.reloadData()
                self.refreshControl?.endRefreshing()
            }
        } else {
            sender.endRefreshing()
        }
    }
}

