//
//  NewsFeedCollectionViewCell.swift
//  Imbanak
//
//  Created by Hovak Davtyan on 12/4/17.
//  Copyright © 2017 alfaSolutions. All rights reserved.
//

import UIKit

class NewsFeedCollectionViewCell: UICollectionViewCell {
   
    @IBOutlet weak var containerView: UIView!
    @IBOutlet weak var imageHeightConstraint: NSLayoutConstraint!
    
    @IBOutlet weak var thumbnailImageView: UIImageView!
    
    @IBOutlet weak var playIcon: UIImageView!
    
    @IBOutlet weak var darkColorBarView: UIView!
    @IBOutlet weak var midColorBarView: UIView!
    @IBOutlet weak var lightColorBarView: UIView!
    
    @IBOutlet weak var titleLabel: UILabel!
    @IBOutlet weak var sourceLabel: UILabel!
    @IBOutlet weak var dateLabel: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        

        self.contentView.autoresizingMask.insert(.flexibleWidth)
        self.contentView.autoresizingMask.insert(.flexibleHeight)
        self.contentView.translatesAutoresizingMaskIntoConstraints = true
    }
    
    override func layoutSubviews() {
        configureCard()
        if sourceLabel.text == "www.youtube.com" {
            
        } else {
            playIcon.isHidden = true
        }
    }
    
    func setArticle(_ article: ArticleResponse.Article) {
        switch article.thumb {
        case .video:
            setColorBar(color: UIColor.piRedBrown)
            sourceLabel.text = "www.youtube.com"
            playIcon.isHidden = false
        case .generic, .gallery:
            switch article.source {
            case .mil:
                setColorBar(color: UIColor.piDarkBlueGrey)
                sourceLabel.text = "www.mil.am"
            case .hayzinvor:
                setColorBar(color: UIColor.piYellowOrange)
                sourceLabel.text = "www.hayzinvor.am"
            default: break
            }
        }
        
        titleLabel.text = article.title
        
        let dateFormatter = DateFormatter()
        dateFormatter.dateStyle = .long
        dateFormatter.locale = Locale(identifier: "hy")
        dateLabel.text = dateFormatter.string(from: article.publishedAt)
        
        let imageURL = article.featuredImage
        thumbnailImageView.kf.indicatorType = .activity
        thumbnailImageView.kf.setImage(with: imageURL)
    }
    
    func configureCard() {
        
        //Corner Radius
        self.containerView.layer.cornerRadius = 5.0
        self.containerView.layer.masksToBounds = true
        
        //Shadows
        self.contentView.layer.shadowColor = UIColor.black.cgColor
        self.contentView.layer.shadowOpacity = 0.16
        self.contentView.layer.shadowOffset = CGSize.zero
        self.contentView.layer.shadowRadius = 0.5
        self.contentView.layer.shadowPath = UIBezierPath(roundedRect: self.contentView.bounds, cornerRadius: 5).cgPath
        self.contentView.layer.masksToBounds = false
        
        self.layer.shadowColor = UIColor.black.cgColor
        self.layer.shadowOpacity = 0.24
        self.layer.shadowOffset = CGSize(width: 0.0, height: 0.5)
        self.layer.shadowRadius = 0.5
        self.layer.shadowPath = UIBezierPath(roundedRect: self.bounds, cornerRadius: 5).cgPath
        self.layer.masksToBounds = false
        
        //Image Constraint
        let contraintConstantRatio: CGFloat = 180 / 343
        let constraintConstant = self.contentView.frame.width * contraintConstantRatio
        self.imageHeightConstraint.constant = constraintConstant
    }
    
    func setColorBar(color: UIColor) {
        //Set Color
        self.darkColorBarView.backgroundColor = color
        self.midColorBarView.backgroundColor = color.withAlphaComponent(0.75)
        self.lightColorBarView.backgroundColor = color.withAlphaComponent(0.60)
        
        //Set Lengths
        let unitLength = self.contentView.frame.width / 6
        self.darkColorBarView.frame.size.width = unitLength * 2
        self.midColorBarView.frame.size.width = unitLength * 1
        self.lightColorBarView.frame.size.width = unitLength * 3
        
        //Set Y positions
        let yPosition = self.contentView.frame.height - 8
        self.darkColorBarView.frame.origin.y = yPosition
        self.midColorBarView.frame.origin.y = yPosition
        self.lightColorBarView.frame.origin.y = yPosition
        
        //Position colorBars randomly
        var colorBarViews = [self.darkColorBarView, self.midColorBarView, self.lightColorBarView]
        colorBarViews[0]?.frame.origin.x = 0
        colorBarViews[1]?.frame.origin.x = (colorBarViews[0]?.frame.width)!
        colorBarViews[2]?.frame.origin.x = (colorBarViews[1]?.frame.width)! + (colorBarViews[0]?.frame.width)!
    }
    
    
}
