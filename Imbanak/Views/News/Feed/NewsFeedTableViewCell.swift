//
//  NewsFeedTableViewCell.swift
//  Imbanak
//
//  Created by Hovak Davtyan on 2/5/18.
//  Copyright © 2018 alfaSolutions. All rights reserved.
//

import UIKit

class NewsFeedTableViewCell: UITableViewCell {
    
    var headline: Headline! {
        didSet {
            setHeadline(headline)
        }
    }
    
    @IBOutlet weak var cardContainerView: UIView!
    
    @IBOutlet weak var imageHeightConstraint: NSLayoutConstraint!
    
    @IBOutlet weak var thumbnailImageView: UIImageView!
    
    @IBOutlet weak var playIcon: UIImageView!
    @IBOutlet weak var defaultImage: UIImageView!
    
    @IBOutlet weak var colorBar: ColorBar!
    
    @IBOutlet weak var titleLabel: UILabel! {
        didSet {
            titleLabel.font = scaledFont.font(forTextStyle: .title3)
        }
    }
    @IBOutlet weak var sourceLabel: UILabel! {
        didSet {
            sourceLabel.font = scaledFont.font(forTextStyle: .subheadline)
        }
    }
    @IBOutlet weak var dateLabel: UILabel! {
        didSet {
            dateLabel.font = scaledFont.font(forTextStyle: .subheadline)
        }
    }
    
    var shouldShimmer = true
    
    private lazy var scaledFont = ScaledFont.init(fontName: "WeeblySleekUI")
    
    override func awakeFromNib() {
        super.awakeFromNib()
        
        self.cardContainerView.layer.cornerRadius = 5.0
        self.cardContainerView.layer.masksToBounds = true
    }
    
    override func layoutSubviews() {
        super.layoutSubviews()
    }
    
    override func prepareForReuse() {
        super.prepareForReuse()
        thumbnailImageView.image = UIImage()
        defaultImage.isHidden = false
        playIcon.isHidden = true
        dateLabel.text = ""
    }
    
    private func setHeadline(_ headline: Headline) {
        
        //Adjustments depending on source
        switch headline.thumb {
        case .video:
            sourceLabel.text = "www.youtube.com"
            playIcon.isHidden = false
        case .generic, .gallery:
            switch headline.source {
            case .mil:
                sourceLabel.text = "www.mil.am"
            case .hayzinvor:
                sourceLabel.text = "www.hayzinvor.am"
            default: break
            }
        }
        
        //Title
        titleLabel.text = headline.title
        dateLabel.text = headline.publishedAt.stringValue
        
        //Image
        let imageURL = headline.featuredImage
        defaultImage.isHidden = false
        thumbnailImageView.startShimmering(withIntensity: 0.6)
        thumbnailImageView.kf.setImage(with: imageURL) { [weak self]
            (image, error, cache, url) in
            self?.thumbnailImageView.stopShimmering()
            self?.defaultImage.isHidden = true
        }
        
        //ColorBar
        switch headline.source {
        case .mil:
            if headline.thumb == .video {
                colorBar.color = .piRedBrown
            } else {
                colorBar.color = .piDarkBlueGrey
            }
        case .hayzinvor:
            colorBar.color = .piYellowOrange
        case .mandatory, .contract, .tactical:
            colorBar.color = .piDarkSage
        case .service, .product, .staff:
            colorBar.color = .piGunmetal
        }
        
        colorBar.id = headline.id
    }
}
