//
//  NewsYoutubePlayerViewController.swift
//  Imbanak
//
//  Created by Hovak Davtyan on 12/25/17.
//  Copyright © 2017 alfaSolutions. All rights reserved.
//

import UIKit
import YoutubeKit
import WebKit
import TwitterKit
import FacebookShare
import FBSDKCoreKit
import FBSDKLoginKit
import FBSDKShareKit
class NewsYoutubePlayerViewController: UIViewController {
    
    //Outlets
    @IBOutlet weak var backgroundView: UIView!
    @IBOutlet weak var thumbnailImageView: UIImageView!
    @IBOutlet weak var activityIndicator: IBActivityIndicator!
    
    //Youtube
    @IBOutlet weak var youtubePlayerView: UIView!
    private var youtubePlayer: YTSwiftyPlayer!
    @IBOutlet weak var controlView: UIView!
    @IBOutlet weak var controlActionView: UIView!
    @IBOutlet weak var mainSlider: YoutubeSlider!
    @IBOutlet weak var bufferSlider: YoutubeSlider!
    @IBOutlet weak var playButton: UIButton!
    @IBOutlet weak var playIcon: UIButton!
    @IBOutlet weak var fullScreenButton: UIButton!
    
    //Labels
    @IBOutlet weak var currentTimeLabel: UILabel!{
        didSet {
            currentTimeLabel.font = scaledFont.font(forTextStyle: .callout)
        }
    }
    @IBOutlet weak var durationLabel: UILabel!{
        didSet {
            durationLabel.font = scaledFont.font(forTextStyle: .callout)
        }
    }
    
    @IBOutlet weak var titleLabel: UILabel! {
        didSet {
            titleLabel.font = scaledFont.font(forTextStyle: .title3)
        }
    }
    @IBOutlet weak var sourceLabel: UILabel! {
        didSet {
            sourceLabel.font = scaledFont.font(forTextStyle: .subheadline)
        }
    }
    @IBOutlet weak var dateLabel: UILabel! {
        didSet {
            dateLabel.font = scaledFont.font(forTextStyle: .subheadline)
        }
    }
    @IBOutlet weak var descriptionLabel: UILabel! {
        didSet {
            descriptionLabel.font = scaledFont.font(forTextStyle: .body)
        }
    }
    @IBOutlet weak var relatedNewsLabel: UILabel! {
        didSet {
            relatedNewsLabel.font = scaledFont.font(forTextStyle: .callout)
        }
    }
    
    //Header and Table View
    @IBOutlet weak var tableView: UITableView!
    @IBOutlet weak var headerView: UIView!
    @IBOutlet weak var seperatorView: UIView!
    
    //Close Button
    @IBOutlet weak var closeButton: UIButton!
    @IBOutlet weak var closeButtonLeadingConstraint: NSLayoutConstraint!
    
    //Layout Constraints
    var heightConstraint: NSLayoutConstraint!
    var centerYConstraint: NSLayoutConstraint!
    
    //GestureRecongnizers
    @IBOutlet var controlViewGestureRecognizer: UITapGestureRecognizer!
    @IBOutlet var controlActionViewGestureRecognizer: UITapGestureRecognizer!
    var doubleTapGestureRecognizer: UITapGestureRecognizer!
    
    //Properties and Flags
    var autohideTask: DispatchWorkItem?
    var playerWasAutoPaused = false
    var playerIsReady = false
    var shouldHideHomeIndicator = false
    var sliderGate = false
    
    
    //Model
    var headline: Headline!
    var article: ArticleWrapper!
    var videoID: String?
    var networkController: NetworkController!
    
    //Font Scale
    private lazy var scaledFont = ScaledFont.init(fontName: "WeeblySleekUI")
    
    //Device Overrides
    override var shouldAutorotate: Bool {
        if playerIsReady {
            return true
        } else {
            return false
        }
    }
    
    override var supportedInterfaceOrientations: UIInterfaceOrientationMask {
        return [.portrait, .landscapeLeft, .landscapeRight]
    }
    
    override func prefersHomeIndicatorAutoHidden() -> Bool
    {
        return shouldHideHomeIndicator
    }
    
    override func childViewControllerForHomeIndicatorAutoHidden() -> UIViewController? {
        return nil
    }
    
    //Related News
    var relatedNews = [Headline]()
    
    //Methods
    private func processArticle(_ article: Article) {
        relatedNews = article.relatedArticles.filter { $0.thumb == .video }
        tableView.reloadData()
        if !relatedNews.isEmpty {
            DispatchQueue.main.async {
                self.seperatorView.isHidden = false
            }
        }
        for block in article.blocks {
            switch block.blockType {
            case .medias:
                for media in block.media {
                    switch media.mediaType {
                    case .video:
                        videoID = media.video?.absoluteString
                    default: break
                    }
                }
            default: break
            }
        }
        guard let videoID = videoID else { return }
        self.loadVideo(videoID: videoID)
    }
    
    private func loadVideo (videoID: String) {
        youtubePlayerView.isHidden = true
        activityIndicator.type = .youtube
        activityIndicator.startAnimating()
        youtubePlayer.loadPlayer()
        
        let request = VideoListRequest(part: [.id, .snippet], filter: .id(videoID))
        ApiSession.shared.send(request) {
            result in
            
            switch result {
            case .success(let response):
                let videoSnippet = response.items.first?.snippet
                //Setup Date
                let dateFormatter = DateFormatter()
                dateFormatter.dateFormat = "yyyy-MM-dd'T'HH:mm:ss.000'Z'"
                dateFormatter.locale = Locale(identifier: "hy")
                let dateString = videoSnippet?.publishedAt
                let date = dateFormatter.date(from: dateString!)
                dateFormatter.dateStyle = .long
                
                DispatchQueue.main.async {
                    self.titleLabel.text = videoSnippet?.title.replacingOccurrences(of: "Զինուժ / Zinuj / Зинуж", with: "Զինուժ")
                    self.thumbnailImageView.kf.setImage(with: URL(string: (videoSnippet?.thumbnails.high.url)!))
                    self.dateLabel.text = dateFormatter.string(from: date!)
                    self.descriptionLabel.text = videoSnippet?.description
                }


            case .failed(let error):
                print(error)
            }
        }
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        tableView.estimatedRowHeight = 107
        tableView.rowHeight = UITableViewAutomaticDimension
        
        //Player
        youtubePlayer = YTSwiftyPlayer(
            frame: CGRect(x: 0, y: 0, width: backgroundView.frame.width, height: backgroundView.frame.height),
            playerVars: [
                .autoplay(true),
                .playsInline(true),
                .showControls(.hidden),
                .showInfo(false),
                .showRelatedVideo(false),
                .showModestbranding(true),
                .showLoadPolicy(false),
                .playerLanguage("hy")
            ]
        )
        
        //Add Youtube Player
        youtubePlayerView.addSubview(youtubePlayer)
        
        youtubePlayer.translatesAutoresizingMaskIntoConstraints = false
        let widthConstraint = NSLayoutConstraint(item: youtubePlayer, attribute: .width,
                                                 relatedBy: .equal,
                                                 toItem: youtubePlayerView, attribute: .width,
                                                 multiplier: 1, constant: 0)
        heightConstraint = NSLayoutConstraint(item: youtubePlayer, attribute: .height,
                                              relatedBy: .equal,
                                              toItem: youtubePlayerView, attribute: .height,
                                              multiplier: 1, constant: 0)
        let centerXConstraint = NSLayoutConstraint(item: youtubePlayer, attribute: .centerX,
                                                   relatedBy: .equal,
                                                   toItem: youtubePlayerView, attribute: .centerX,
                                                   multiplier: 1, constant: 0)
        centerYConstraint = NSLayoutConstraint(item: youtubePlayer, attribute: .centerY,
                                               relatedBy: .equal,
                                               toItem: youtubePlayerView, attribute: .centerY,
                                               multiplier: 1, constant: 0)
        youtubePlayerView.addConstraints([widthConstraint, heightConstraint, centerXConstraint, centerYConstraint])
        
        youtubePlayer.delegate = self
        
        //Get Content
        if let videoID = videoID {
            loadVideo(videoID: videoID)
        } else {
            networkController.loadArticle(byID: headline.id) { [weak self]
                article in
                guard let `self` = self else { return }
                guard let article = article else { return }
                self.processArticle(article)
            }
        }
        
        //Slider
        mainSlider.hideThumb()
        bufferSlider.hideThumb()
        mainSlider.isUserInteractionEnabled = false
        
        controlView.addObserver(self, forKeyPath: "isHidden", options: .new, context: nil)
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        if view.safeAreaInsets.top > 0 || view.safeAreaInsets.left > 0 {
            UIApplication.shared.isStatusBarHidden = false
            UIApplication.shared.statusBarStyle = .lightContent
        } else {
            UIApplication.shared.isStatusBarHidden = true
        }
    }
    
    override func viewWillLayoutSubviews() {
        super.viewWillLayoutSubviews()
        //TableView Header
            let headerSize = headerView.systemLayoutSizeFitting(UILayoutFittingExpandedSize)
            if headerView.frame.size.height != headerSize.height {
                headerView.frame.size.height = headerSize.height
                tableView.tableHeaderView = headerView
                tableView.layoutIfNeeded()
            }
    }
    
    override func viewWillTransition(to size: CGSize, with coordinator: UIViewControllerTransitionCoordinator) {
        
        if UIDevice.current.orientation != .portrait {
            
            //If Device Has Notch
            if view.safeAreaInsets.top > 0 {
                heightConstraint.constant += view.safeAreaInsets.top / 2
                centerYConstraint.constant += view.safeAreaInsets.top / 4
                doubleTapGestureRecognizer = UITapGestureRecognizer(target: self, action: #selector(videoDoubleTapped))
                doubleTapGestureRecognizer.numberOfTapsRequired = 2
                controlActionViewGestureRecognizer.require(toFail: doubleTapGestureRecognizer)
                controlActionView.addGestureRecognizer(doubleTapGestureRecognizer)
                shouldHideHomeIndicator = true
                setNeedsUpdateOfHomeIndicatorAutoHidden()
                youtubePlayerView.setNeedsLayout()
                youtubePlayer.setNeedsLayout()
            }
            
            if let image = UIImage(named: "ExitFullScreenButton") {
                fullScreenButton.setImage(image, for: .normal)
            }
            
            if controlView.isHidden {
                mainSlider.isHidden = true
                bufferSlider.isHidden = true
            }
            

        } else {
            
            //If Device Has Notch
            if view.safeAreaInsets.left > 0 {
                heightConstraint.constant -= view.safeAreaInsets.left / 2
                centerYConstraint.constant -= view.safeAreaInsets.left / 4
                controlActionView.removeGestureRecognizer(doubleTapGestureRecognizer)
                shouldHideHomeIndicator = false
                setNeedsUpdateOfHomeIndicatorAutoHidden()
                youtubePlayerView.setNeedsLayout()
                youtubePlayer.setNeedsLayout()
                if youtubePlayer.isEnlarged {
                    youtubePlayer.shrinkVideo()
                }
            }
            
            if let image = UIImage(named: "EnterFullScreenButton") {
                fullScreenButton.setImage(image, for: .normal)
            }
            
            if controlView.isHidden {
                mainSlider.isHidden = true
                bufferSlider.isHidden = true
            }
        }
    }
    
    @IBAction func play(_ sender: UIButton) {
        if playerIsReady {
            if youtubePlayer.playerState == .playing {
                youtubePlayer.pauseVideo()
            } else {
                youtubePlayer.playVideo()
            }
        }
    }
    
    @IBAction func playIconTapped(_ sender: UIButton) {
        youtubePlayer.playVideo()
        controlActionView.isHidden = false
        mainSlider.hideThumb()
        mainSlider.value = 0
        mainSlider.isHidden = false
        bufferSlider.isHidden = false
        youtubePlayerView.isHidden = false
    }
    
    
    @IBAction func toggleFullScreen(_ sender: UIButton) {
        let currentOrientation = UIDevice.current.orientation
        
        if currentOrientation == .portrait {
            let value = UIInterfaceOrientation.landscapeRight.rawValue
            UIDevice.current.setValue(value, forKey: "orientation")
        } else {
            let value = UIInterfaceOrientation.portrait.rawValue
            UIDevice.current.setValue(value, forKey: "orientation")
        }
    }
    
    
    @IBAction func taped(_ sender: UITapGestureRecognizer) {
        if playerIsReady {
            autohideTask?.cancel()
            if controlView.isHidden {
                autohideTask = autoHideTask()
                controlView.isHidden = false
                controlActionView.isHidden = true
                
                mainSlider.isHidden = false
                bufferSlider.isHidden = false
                
                mainSlider.showThumb()
                mainSlider.isUserInteractionEnabled = true
                DispatchQueue.main.asyncAfter(wallDeadline: .now() + 3, execute: autohideTask!)
            } else {
                controlView.isHidden = true
                controlActionView.isHidden = false
                if UIDevice.current.orientation != .portrait {
                    mainSlider.isHidden = true
                    bufferSlider.isHidden = true
                }
                mainSlider.hideThumb()
                mainSlider.isUserInteractionEnabled = false
            }
        }
    }
    
    @objc func videoDoubleTapped() {
        if playerIsReady {
            if youtubePlayer.isEnlarged {
                youtubePlayer.shrinkVideo()
            } else {
                youtubePlayer.enlargeVideo()
            }
        }
    }
    
    @IBAction func closeButton(_ sender: UIButton) {
        self.dismiss(animated: true, completion: nil)
        UIApplication.shared.statusBarStyle = .default
        UIApplication.shared.isStatusBarHidden = false
    }
    
    //MARK: Social Actions
    
    @IBAction func shareTwitterDidTap(_ sender: UIButton) {
        guard let url = URL(string: "https://www.youtube.com/watch?v=" + videoID!) else { return }
        if (UIApplication.shared.canOpenURL(URL(string:"twitter://")!)) {
            if (TWTRTwitter.sharedInstance().sessionStore.hasLoggedInUsers()) {
                // App must have at least one logged-in user to compose a Tweet
                let composer = TWTRComposerViewController.init(initialText: url.absoluteString, image: nil, videoData: nil)
                present(composer, animated: true, completion: nil)
            } else {
                // Log in, and then check again
                TWTRTwitter.sharedInstance().logIn { session, error in
                    if session != nil { // Log in succeeded
                        let composer = TWTRComposerViewController.init(initialText: url.absoluteString, image: nil, videoData: nil)
                        self.present(composer, animated: true, completion: nil)
                    } else {
                        let alert = UIAlertController(title: "No Twitter Accounts Available", message: "You must log in to continue.", preferredStyle: .alert)
                        alert.addAction(UIAlertAction.init(title: "OK", style: .default, handler: nil))
                        self.present(alert, animated: false, completion: nil)
                    }
                }
            }
        } else {
            let alert = UIAlertController(title: "Twitter App Not Found", message: "You must install the Twitter App to continue.", preferredStyle: .alert)
            alert.addAction(UIAlertAction.init(title: "OK", style: .default, handler: nil))
            self.present(alert, animated: false, completion: nil)
        }
    }
    
    @IBAction func shareFacebookDidTap(_ sender: UIButton) {
//        guard let url = URL(string: "https://www.youtube.com/watch?v=" + videoID!) else { return }
//        let shareContent = LinkShareContent(url: url)
//        let shareDialog = ShareDialog(content: shareContent)
//        shareDialog.mode = .automatic
//        shareDialog.presentingViewController = self
//        shareDialog.failsOnInvalidData = true
//        shareDialog.completion = { result in
//            // Handle share results
//        }
//        do {
//        try shareDialog.show()
//        } catch {
//            print(url)
//                print(error.localizedDescription)
//            print(shareDialog)
//        }
    }
    
    
    func secondsToHoursMinutesSeconds (seconds : Int) -> String {
        let currentTimeTuple = (hours: seconds / 3600, minutes: (seconds % 3600) / 60,seconds: (seconds % 3600) % 60)
        
        let curentTimeHoursString = String(currentTimeTuple.hours)
        let currentTimeMinutesString = String(currentTimeTuple.minutes)
        var currentTimeSecondsString = String(currentTimeTuple.seconds)
        
        if currentTimeTuple.seconds < 10 {
            currentTimeSecondsString = "0" + currentTimeSecondsString
        }
        
        var currentTimeString: String
        
        if currentTimeTuple.hours == 0 {
            currentTimeString = currentTimeMinutesString + ":" + currentTimeSecondsString
        } else {
            currentTimeString = curentTimeHoursString + ":" + currentTimeMinutesString + ":" + currentTimeSecondsString
        }
        
        return currentTimeString
    }
    
    func autoHideTask() -> DispatchWorkItem? {
        
        let task = DispatchWorkItem {
            if self.youtubePlayer.playerState == .playing && !self.controlView.isHidden {
                self.controlView.isHidden = true
                self.controlActionView.isHidden = false
                if UIDevice.current.orientation != .portrait {
                    self.mainSlider.isHidden = true
                    self.bufferSlider.isHidden = true
                }
                self.mainSlider.hideThumb()
                self.mainSlider.isUserInteractionEnabled = false
                
            }
        }
        
        return task
    }
    
    
    
    //MARK: Slider
    @IBAction func manualSlide(_ sender: YoutubeSlider) {
        if mainSlider.value >= mainSlider.maximumValue {
            youtubePlayer.stopVideo()
            playerWasAutoPaused = false
        } else if !sliderGate {
            if youtubePlayer.playerState == .playing {
                playButton.isHidden = true
                controlActionView.isHidden = false
                youtubePlayer.pauseVideo()
                sender.showThumb()
                playerWasAutoPaused = true
            } else if youtubePlayer.playerState == .paused {
                playerWasAutoPaused = false
            }
            sliderGate = true
        } else {
            youtubePlayer.seek(to: Int(sender.value), allowSeekAhead: true)
            currentTimeLabel.text = secondsToHoursMinutesSeconds(seconds: Int(sender.value))
        }
    }
    
    @IBAction func sliderEditingDidEnd(_ sender: YoutubeSlider) {
        if mainSlider.value >= mainSlider.maximumValue {
            youtubePlayer.stopVideo()
            playerWasAutoPaused = false
        } else if playerWasAutoPaused {
            if let actionImage = UIImage(named: "pauseButton") {
                playButton.setImage(actionImage, for: .normal)
            }
            youtubePlayer.playVideo()
        }
        
        mainSlider.isUserInteractionEnabled = true
        
        playButton.isHidden = false
        controlActionView.isHidden = true
        sender.showThumb()
        
        sliderGate = false
    }
}

//MARK: - Player Delegate

extension NewsYoutubePlayerViewController: YTSwiftyPlayerDelegate {
    
    func playerReady(_ player: YTSwiftyPlayer) {
        guard let videoID = videoID else { return }
        player.loadVideo(videoID: videoID)

    }
    
    func player(_ player: YTSwiftyPlayer, didChangeState state: YTSwiftyPlayerState) {
        
        switch state {
        case .playing:
            if activityIndicator.isAnimating {
                playerIsReady = true
                activityIndicator.stopAnimating()
                youtubePlayerView.isHidden = false
                mainSlider.isHidden = false
                bufferSlider.isHidden = false
                backgroundView.backgroundColor = UIColor.white
            }

            if let actionImage = UIImage(named: "pauseButton") {
                playButton.setImage(actionImage, for: .normal)
            }
            
            autohideTask = autoHideTask()
            DispatchQueue.main.asyncAfter(wallDeadline: .now() + 3, execute: autohideTask!)
        case .paused:
            if let actionImage = UIImage(named: "playButton") {
                playButton.setImage(actionImage, for: .normal)
            }
            autoHideTask()?.cancel()
        case .ended:
            youtubePlayerView.isHidden = true
            controlActionView.isHidden = true
            controlView.isHidden = true
            mainSlider.isHidden = true
            bufferSlider.isHidden = true
            
            playIcon.isHidden = false
        default: break
        }
    }
    
    func player(_ player: YTSwiftyPlayer, didUpdateCurrentTime currentTime: Double) {
        if !sliderGate {
            mainSlider.value = Float(currentTime)
            currentTimeLabel.text = secondsToHoursMinutesSeconds(seconds: Int(currentTime))
        }
    }
    
    func player(_ player: YTSwiftyPlayer, didGetDuration duration: Double) {
        mainSlider.maximumValue = Float(duration)
        durationLabel.text = self.secondsToHoursMinutesSeconds(seconds: Int(duration))
    }
    
    func player(_ player: YTSwiftyPlayer, didUpdateBufferedVideoRate bufferedVideoRate: Double) {
        bufferSlider.value = Float(youtubePlayer.bufferedVideoRate)
    }
    
    func player(_ player: YTSwiftyPlayer, didChangePlaybackRate playbackRate: Double) {}
    
    func player(_ player: YTSwiftyPlayer, didChangeQuality quality: YTSwiftyVideoQuality) {}
    
    func apiDidChange(_ player: YTSwiftyPlayer) {}
    
    func youtubeIframeAPIReady(_ player: YTSwiftyPlayer) {}
    
    func player(_ player: YTSwiftyPlayer, didReceiveError error: YTSwiftyPlayerError) {
        print("Youtube Player Error:\(error)")
    }
    
    func youtubeIframeAPIFailedToLoad(_ player: YTSwiftyPlayer) {
        print("Api Failed to Load")
    }
}

extension YTSwiftyPlayer {
    var isEnlarged: Bool {
        if self.transform.isIdentity {
            return false
        } else {
            return true
        }
    }
    
    func enlargeVideo () {
        guard let superview = superview else { return }
        let aspect: CGFloat = 16 / 9
        let actualHeight = superview.frame.height * aspect
        let scaleFactor = superview.frame.width / actualHeight
        self.transform = CGAffineTransform(scaleX: scaleFactor, y: scaleFactor)
    }
    
    func shrinkVideo () {
        self.transform = CGAffineTransform.identity
    }
}

extension NewsYoutubePlayerViewController: UITableViewDataSource, UITableViewDelegate {
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return relatedNews.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "relatedNewsCell", for: indexPath) as! NewsArticleRelatedNewsTableViewCell
        let relatedHeadline = relatedNews[indexPath.row]
        cell.headline = relatedHeadline
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let relatedHeadline = relatedNews[indexPath.row]
        switch relatedHeadline.thumb {
        case .video:
            networkController.loadArticle(byID: relatedHeadline.id) { [weak self]
                article in
                guard let `self` = self else { return }
                guard let article = article else { return }
                self.processArticle(article)
            }
        default: break
        }
    }
}
