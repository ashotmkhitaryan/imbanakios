//
//  NewsArticleGalleryPageViewController.swift
//  Imbanak
//
//  Created by Hovak Davtyan on 12/20/17.
//  Copyright © 2017 alfaSolutions. All rights reserved.
//

import UIKit

class NewsArticleGalleryPageViewController: UIPageViewController {
    
    @IBOutlet var containerView: UIView!
    @IBOutlet weak var pageControlViewContainer: UIView!
    @IBOutlet weak var closeButton: UIButton!
    @IBOutlet weak var saveButton: UIButton!
    @IBOutlet weak var labelContainerView: UIView!

    @IBOutlet weak var homeIndicatorBackground: UIView!
    @IBOutlet weak var descriptionLabel: UILabel!
    @IBOutlet weak var titleLabel: UILabel!
    @IBOutlet weak var titleTopLayoutConstraint: NSLayoutConstraint!
    @IBOutlet weak var descriptionBottomLayoutConstraint: NSLayoutConstraint!
    
    var pageControlView: ColorBarPageControlView!
    var color: UIColor!
    
    var lastPendingViewControllerIndex: Int?
    var currentPageIndex: Int?
    var darkModeEnabled = false
    var shouldHideHomeIndicator = false
    
    var medias = [Article.Block.Media]()
    
    var selectedImageIndex: Int!
    
    override func prefersHomeIndicatorAutoHidden() -> Bool
    {
        return shouldHideHomeIndicator
    }
    
    override func childViewControllerForHomeIndicatorAutoHidden() -> UIViewController? {
        return nil
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        containerView.frame = view.frame
        view.addSubview(containerView)
        
        self.view.backgroundColor = UIColor.white
        
        if medias.count > 1 {
            pageControlView = ColorBarPageControlView(frame: containerView.frame, startIndex: selectedImageIndex, color: color, numberOfElements: medias.count)
            pageControlViewContainer.addSubview(pageControlView)
        } else {
            let colorBar = ColorBar(frame: CGRect(x: 0, y: 0, width: containerView.frame.width, height: 8.0))
            colorBar.id = medias.first?.id
            colorBar.color = color
            colorBar.autoresizingMask = [.flexibleHeight, .flexibleWidth]
            pageControlViewContainer.addSubview(colorBar)
        }

        
        if let viewController = setGalleryImageViewController(selectedImageIndex ?? 0) {
            let viewControllers = [viewController]
            setViewControllers(viewControllers,
                               direction: .forward,
                               animated: false,
                               completion: nil)
        }
        
        dataSource = self
        delegate = self
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)

    }
    
    func setGalleryImageViewController(_ index:Int) -> NewsArticleGalleryImageViewController? {
        guard let storyboard = storyboard,
            let page = storyboard.instantiateViewController(withIdentifier: "NewsArticleGalleryImageViewController") as? NewsArticleGalleryImageViewController
            else {return nil}
        page.media = medias[index]
        page.imageIndex = index
        return page
    }
    
    
    // MARK: - Functions
    
    internal func enableDarkMode() {
        if  !darkModeEnabled {
            darkModeEnabled = true
            
            print("willEnableDarkMode")
            darkModeEnabled = true
            
            UIApplication.shared.isStatusBarHidden = true
            view.backgroundColor = UIColor.black
            pageControlViewContainer.isHidden = true
            labelContainerView.isHidden = true
            homeIndicatorBackground.isHidden = true
            closeButton.isHidden = true
            saveButton.isHidden = true
            shouldHideHomeIndicator = true
            setNeedsUpdateOfHomeIndicatorAutoHidden()
        }
    }
    
    internal func disableDarkMode() {
        if  darkModeEnabled {
            
            darkModeEnabled = false
            
            UIApplication.shared.isStatusBarHidden = false
            view.backgroundColor = UIColor.white
            pageControlViewContainer.isHidden = false
            labelContainerView.isHidden = false
            homeIndicatorBackground.isHidden = false
            closeButton.isHidden = false
            saveButton.isHidden = false
            shouldHideHomeIndicator = false
            setNeedsUpdateOfHomeIndicatorAutoHidden()
        }
    }
    
}

extension NewsArticleGalleryPageViewController: UIPageViewControllerDataSource, UIPageViewControllerDelegate {
    
    //Data Source
    func pageViewController(_ pageViewController: UIPageViewController, viewControllerBefore viewController: UIViewController) -> UIViewController? {
        
        if let viewController = viewController as? NewsArticleGalleryImageViewController {
            if let index = viewController.imageIndex,
                index > 0 {
                return setGalleryImageViewController(index - 1)
            }
        }
        
        return nil
    }
    
    func pageViewController(_ pageViewController: UIPageViewController, viewControllerAfter viewController: UIViewController) -> UIViewController? {
        if let viewController = viewController as? NewsArticleGalleryImageViewController {
            if let index = viewController.imageIndex,
                index + 1 < medias.count {
                return setGalleryImageViewController(index + 1)
            }
        }
        
        return nil
    }
    
    func pageViewController(_ pageViewController: UIPageViewController, willTransitionTo pendingViewControllers: [UIViewController]) {
        if let viewController = pendingViewControllers[0] as? NewsArticleGalleryImageViewController {
            self.lastPendingViewControllerIndex = viewController.imageIndex
        }
    }
    
    func pageViewController(_ pageViewController: UIPageViewController, didFinishAnimating finished: Bool, previousViewControllers: [UIViewController], transitionCompleted completed: Bool) {
        
        if completed {
            self.currentPageIndex = self.lastPendingViewControllerIndex
            pageControlView?.adjustIndicator(position: self.currentPageIndex!)
        }
    }
}
