//
//  NewsArticleGallerCollectionViewCell.swift
//  Imbanak
//
//  Created by Hovak Davtyan on 12/18/17.
//  Copyright © 2017 alfaSolutions. All rights reserved.
//

import UIKit

class NewsArticleGallerCollectionViewCell: UICollectionViewCell {
    @IBOutlet weak var imageView: UIImageView!
    @IBOutlet weak var defaultImage: UIImageView!
    var medias = [Article.Block.Media]()
    var index: Int!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        //Corner Radius
        imageView.layer.cornerRadius = 2.5
        imageView.layer.masksToBounds = true
    }
}

