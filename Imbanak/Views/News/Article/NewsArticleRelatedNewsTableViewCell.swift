//
//  NewsArticleRelatedNewsTableViewCell.swift
//  Imbanak
//
//  Created by Hovak Davtyan on 12/19/17.
//  Copyright © 2017 alfaSolutions. All rights reserved.
//

import UIKit

class NewsArticleRelatedNewsTableViewCell: UITableViewCell {
    
    @IBOutlet weak var cardView: UIView!
    @IBOutlet weak var colorBar: ColorBar!
    
    @IBOutlet weak var featuredImageView: UIImageView!
    @IBOutlet weak var defaultImageView: UIImageView!
    @IBOutlet weak var titleLabel: UILabel!
    @IBOutlet weak var sourceLabel: UILabel!
    @IBOutlet weak var dateLabel: UILabel!
    
    var headline: Headline! {
        didSet {
            setHeadline(headline)
        }
    }
    
    let cardInset: CGFloat = 16
    
    var cardConfigured = false
    
    private lazy var scaledFont = ScaledFont(fontName: "WeeblySleekUI")
    
    override func awakeFromNib() {
        super.awakeFromNib()
        
        self.cardView.layer.cornerRadius = 5.0
        self.cardView.layer.masksToBounds = true
    }
    
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
    }
    
    private func setHeadline(_ headline: Headline) {
        
        //Adjustments depending on source
        switch headline.thumb {
        case .video:
            sourceLabel.text = "www.youtube.com"
        case .generic, .gallery:
            switch headline.source {
            case .mil:
                sourceLabel.text = "www.mil.am"
            case .hayzinvor:
                sourceLabel.text = "www.hayzinvor.am"
            default: break
            }
        }
        
        sourceLabel.font = scaledFont.font(forTextStyle: .caption1)
        
        //Title
        titleLabel.text = headline.title
        titleLabel.font = scaledFont.font(forTextStyle: .callout)
        
        //Date
        let dateFormatter = DateFormatter()
        dateFormatter.dateStyle = .long
        dateFormatter.locale = Locale(identifier: "hy")
        
        let calendar = Calendar.current
        
        //Current Date components
        let current = Date()
        let currentYear = calendar.component(.year, from: current)
        let currentMonth = calendar.component(.month, from: current)
        let currentDay = calendar.component(.day, from: current)
        
        //Article date components
        let articleYear = calendar.component(.year, from: headline.publishedAt)
        if articleYear == currentYear {
            let articleMonth = calendar.component(.month, from: headline.publishedAt)
            if articleMonth == currentMonth {
                let articleDay = calendar.component(.day, from: headline.publishedAt)
                if articleDay == currentDay {
                    dateLabel.text = "Այսօր"
                } else if articleDay == currentDay - 1 {
                    dateLabel.text = "Երեկ"
                }
            }
        }
        
        if (dateLabel.text?.isEmpty)! {
            dateLabel.text = dateFormatter.string(from: headline.publishedAt)
        }
        
        dateLabel.font = scaledFont.font(forTextStyle: .caption1)
        
        //Image
        let imageURL = headline.featuredImage
        featuredImageView.startShimmering(withIntensity: 0.6)
        featuredImageView.kf.setImage(with: imageURL) { [weak self]
            (image, error, cache, url) in
            self?.defaultImageView.isHidden = true
            self?.featuredImageView.stopShimmering()
        }
        
        //ColorBar
        switch headline.source {
        case .mil:
            if headline.thumb == .video {
                colorBar.color = .piRedBrown
            } else {
                colorBar.color = .piDarkBlueGrey
            }
        case .hayzinvor:
            colorBar.color = .piYellowOrange
        case .mandatory, .contract, .tactical:
            colorBar.color = .piDarkSage
        case .service, .product, .staff:
            colorBar.color = .piGunmetal
        }
        
        colorBar.id = headline.id
    }
}
