//
//  NewsArticleTextTableViewCell.swift
//  Imbanak
//
//  Created by Hovak Davtyan on 12/17/17.
//  Copyright © 2017 alfaSolutions. All rights reserved.
//

import UIKit

class NewsArticleTextTableViewCell: UITableViewCell {
    @IBOutlet weak var blockTitleLabel: UILabel!
    @IBOutlet weak var blockSubtitleLabel: UILabel!
    
    @IBOutlet weak var articleTextView: UITextView!
    @IBOutlet weak var topConstraint: NSLayoutConstraint!
    @IBOutlet weak var bottomConstraint: NSLayoutConstraint!
    
}
