//
//  NewsArticleGalleryImageViewController.swift
//  Imbanak
//
//  Created by Hovak Davtyan on 12/20/17.
//  Copyright © 2017 alfaSolutions. All rights reserved.
//

import UIKit
import Kingfisher

class NewsArticleGalleryImageViewController: UIViewController {
    
    var imageIndex: Int?
    var media: Article.Block.Media!
    var imageYOffset: CGFloat!
    var maximumZoomScale: CGFloat!
    var minimumZoomScale: CGFloat!
    
    @IBOutlet weak var scrollView: UIScrollView!
    @IBOutlet weak var galleryImageView: UIImageView!
    @IBOutlet weak var gradientOverlay: UIImageView!
    
    @IBOutlet weak var imageHeightConstraint: NSLayoutConstraint!
    @IBOutlet weak var imageWidthConstraint: NSLayoutConstraint!
    @IBOutlet weak var imageTopConstraint: NSLayoutConstraint!
    @IBOutlet weak var imageBottomConstraint: NSLayoutConstraint!
    @IBOutlet weak var imageLeadingConstraint: NSLayoutConstraint!
    @IBOutlet weak var imageTrailingConstraint: NSLayoutConstraint!
    @IBOutlet weak var activityIndicator: IBActivityIndicator!
    
    //Flags
    var isZoomed = false
    
    //Gesture Recognizers
    var tapGestureRecognizer: UITapGestureRecognizer!
    var doubleTapGestureRecogniser: UITapGestureRecognizer!
    
    weak var parentPageViewController: NewsArticleGalleryPageViewController!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        scrollView.contentInsetAdjustmentBehavior = .never
        parentPageViewController = parent as! NewsArticleGalleryPageViewController
        switch parentPageViewController.color! {
        case .piDarkBlueGrey: activityIndicator.type = .mil
        case .piYellowOrange: activityIndicator.type = .hayzinvor
        default: break
        }
        activityIndicator.startAnimating()
        //Set image
        galleryImageView.kf.setImage(with: media.image, completionHandler: { (image,error, cacheType, url) in
            if let image = image {
                self.activityIndicator.stopAnimating()
                let scale = image.scale
                self.imageWidthConstraint.constant = image.size.width * scale
                self.imageHeightConstraint.constant = image.size.height * scale
                self.updateMinZoomScaleForSize(self.view.bounds.size)
                self.updateConstraintsForSize(self.view.bounds.size)
                self.scrollView.setZoomScale(self.minimumZoomScale, animated: false)
                self.view.setNeedsLayout()
                self.view.layoutIfNeeded()
            }
        })
        
        //Gesture Recognizers
        doubleTapGestureRecogniser = UITapGestureRecognizer(target: self, action: #selector(imageDoubleTapped))
        doubleTapGestureRecogniser.numberOfTapsRequired = 2
        
        tapGestureRecognizer = UITapGestureRecognizer(target: self, action: #selector(imageTapped))
        tapGestureRecognizer.require(toFail: doubleTapGestureRecogniser)
        
        galleryImageView.addGestureRecognizer(doubleTapGestureRecogniser)
        view.addGestureRecognizer(tapGestureRecognizer)
        
        //Delegates
        scrollView.delegate = self
    }
    
    override func viewWillLayoutSubviews() {
        super.viewWillLayoutSubviews()
        
        updateMinZoomScaleForSize(view.bounds.size)
        updateConstraintsForSize(view.bounds.size)
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        self.updateMinZoomScaleForSize(self.view.bounds.size)
        self.updateConstraintsForSize(self.view.bounds.size)
        self.scrollView.setZoomScale(self.minimumZoomScale, animated: false)
        print("viewWillAppear")
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        parentPageViewController.descriptionLabel.text = media.description
        parentPageViewController.titleLabel.text = media.title
        
        if media.title == "" && media.description == "" {
            parentPageViewController.titleTopLayoutConstraint.constant = 0
            parentPageViewController.descriptionBottomLayoutConstraint.constant = 0
        }
    }
    
    override func viewWillTransition(to size: CGSize, with coordinator: UIViewControllerTransitionCoordinator) {
        parentPageViewController.containerView.frame.size = size
        parentPageViewController.pageControlView.frame.size.width = size.width
        parentPageViewController.pageControlView.adjustIndicator(position: parentPageViewController.currentPageIndex ?? 0)
        
        updateConstraintsForSize(size)
    }
    
    fileprivate func updateMinZoomScaleForSize(_ size: CGSize) {
        let widthScale = size.width / galleryImageView.bounds.width
        let heightScale = size.height / galleryImageView.bounds.height
        minimumZoomScale = min(widthScale, heightScale)
        
        
        maximumZoomScale = 1 / (galleryImageView.bounds.height / size.height)
        
        scrollView.minimumZoomScale = minimumZoomScale
        
        if !isZoomed {
            scrollView.zoomScale = minimumZoomScale
        }
        
        if let imageSize = galleryImageView.image?.size {
            if imageSize.width < imageSize.height {
                maximumZoomScale = maximumZoomScale + maximumZoomScale / 2
            }
        }
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    func updateConstraintsForSize(_ size: CGSize) {
        imageYOffset = max(0, (size.height - galleryImageView.frame.height) / 2)
        imageTopConstraint.constant = imageYOffset
        imageBottomConstraint.constant = imageYOffset
        
        let xOffset = max(0, (size.width - galleryImageView.frame.width) / 2)
        imageLeadingConstraint.constant = xOffset
        imageTrailingConstraint.constant = xOffset
        
        setGradient()
        view.layoutIfNeeded()
    }
    
    @objc func imageTapped() {
        if parentPageViewController.darkModeEnabled {
            parentPageViewController.disableDarkMode()
        } else {
            parentPageViewController.enableDarkMode()
        }
        setGradient()
    }
    
    @objc func imageDoubleTapped() {
        if !isZoomed {
            parentPageViewController.enableDarkMode()
            if UIDevice.current.orientation != .portrait {
                scrollView.maximumZoomScale = maximumZoomScale + maximumZoomScale / 100 * 60
            } else {
                scrollView.maximumZoomScale = maximumZoomScale
            }
            let location = doubleTapGestureRecogniser.location(in: galleryImageView)
            let locationRect = CGRect(x: location.x, y: location.y, width: 0, height: 0)
            scrollView.zoom(to: locationRect, animated: true)
        } else {
            scrollView.setZoomScale(0.1, animated: true) //Here a trick for setting zoom scale lower than the minimum UNSAFE
        }
    }
    
    func setGradient () {
        if let parent = parentPageViewController {
            if UIDevice.current.orientation == .portrait {
                if imageYOffset < 50 && !parent.darkModeEnabled {
                    //print("unhideGradient")
                    if let closeButtonImage = UIImage(named: "closeArrowWhite") {
                        parent.closeButton.setImage(closeButtonImage, for: .normal)
                    }
                    
                    if let saveButtonImage = UIImage(named: "saveButtonWhite") {
                        parent.saveButton.setImage(saveButtonImage, for: .normal)
                    }
                    
                    gradientOverlay.isHidden = false
                } else {
                    if let closeButtonImage = UIImage(named: "closeArrowBlack") {
                        parent.closeButton.setImage(closeButtonImage, for: .normal)
                    }
                    
                    if let saveButtonImage = UIImage(named: "saveButtonBlack") {
                        parent.saveButton.setImage(saveButtonImage, for: .normal)
                    }
                    gradientOverlay.isHidden = true
                }
            } else {
                gradientOverlay.isHidden = true
                if let closeButtonImage = UIImage(named: "closeArrowBlack") {
                    parent.closeButton.setImage(closeButtonImage, for: .normal)
                }
                
                if let saveButtonImage = UIImage(named: "saveButtonBlack") {
                    parent.saveButton.setImage(saveButtonImage, for: .normal)
                }
            }
        }
    }
    
    @IBAction func saveImage(_ sender: UIButton) {
        if let image = galleryImageView.image {
            UIImageWriteToSavedPhotosAlbum(image, self, #selector(image(_:didFinishSavingWithError:contextInfo:)), nil)
        }
    }
    
    @objc func image(_ image: UIImage, didFinishSavingWithError error: NSError?, contextInfo: UnsafeRawPointer) {
        if let error = error {
            // we got back an error!
            let ac = UIAlertController(title: "Save error", message: error.localizedDescription, preferredStyle: .alert)
            ac.addAction(UIAlertAction(title: "OK", style: .default))
            present(ac, animated: true)
        } else {
            let ac = UIAlertController(title: "", message: "Նկարը հաջողությամբ պահպանվել է Ձեր ֆոտոդարանում։", preferredStyle: .alert)
            ac.addAction(UIAlertAction(title: "Լավ", style: .default))
            present(ac, animated: true)
        }
    }
    
    @IBAction func popViewController(_ sender: Any) {
        if !parentPageViewController.darkModeEnabled {
            UIApplication.shared.isStatusBarHidden = false
            dismiss(animated: true, completion: nil)
        }
    }
}

extension NewsArticleGalleryImageViewController: UIScrollViewDelegate {
    
    func viewForZooming(in scrollView: UIScrollView) -> UIView? {
        return galleryImageView
    }
    
    func scrollViewWillBeginZooming(_ scrollView: UIScrollView, with view: UIView?) {
        
        if UIDevice.current.orientation != .portrait {
            scrollView.maximumZoomScale = maximumZoomScale * 2
        } else {
            scrollView.maximumZoomScale = maximumZoomScale + maximumZoomScale / 100 * 20
        }
    }
    
    func scrollViewDidZoom(_ scrollView: UIScrollView) {
        isZoomed = true
        
        if scrollView.zoomScale == scrollView.minimumZoomScale {
            
            if UIDevice.current.orientation != .portrait {
                scrollView.maximumZoomScale = maximumZoomScale + maximumZoomScale / 100 * 60
            } else {
                scrollView.maximumZoomScale = maximumZoomScale
            }
            isZoomed = false
        }
        updateConstraintsForSize(view.bounds.size)
    }
}

extension IBActivityIndicator: Indicator {
    func startAnimatingView() {
        self.startAnimating()
    }
    
    func stopAnimatingView() {
        stopAnimating()
    }
    
    var view: IndicatorView {
        return self
    }
}
