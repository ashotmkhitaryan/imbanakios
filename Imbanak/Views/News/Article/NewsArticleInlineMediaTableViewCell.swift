//
//  NewsArticleInlineMediaTableViewCell.swift
//  Imbanak
//
//  Created by Hovak Davtyan on 12/18/17.
//  Copyright © 2017 alfaSolutions. All rights reserved.
//

import UIKit
import Kingfisher

class NewsArticleInlineMediaTableViewCell: UITableViewCell {
    
    @IBOutlet weak var titleContainerView: UIView!
    @IBOutlet weak var titleContainerViewBottomConstraint: NSLayoutConstraint!
    @IBOutlet weak var titleContainerViewTopConstraint: NSLayoutConstraint!
    
    @IBOutlet weak var blockTitleLabel: UILabel!
    @IBOutlet weak var blockSubtitleLabel: UILabel!
    
    @IBOutlet weak var imageContainerView: UIImageView!
    @IBOutlet weak var imageContainerViewHeightConstraint: NSLayoutConstraint!
    
    @IBOutlet weak var defaultImage: UIImageView!
    @IBOutlet weak var playIcon: UIImageView!
    
    @IBOutlet weak var descriptionLabel: UILabel!
    @IBOutlet weak var descriptionLabelTopConstraint: NSLayoutConstraint!
    @IBOutlet weak var descriptionLabelBottomConstraint: NSLayoutConstraint!
    
    @IBOutlet weak var titleLabel: UILabel!
    
    @IBOutlet weak var cardView: UIView!
    @IBOutlet weak var colorBar: ColorBar!
    var color: UIColor!
    
    var media: Article.Block.Media! {
        didSet {
            set(media: media)
        }
    }
    
    override func awakeFromNib() {
        super.awakeFromNib()
        
        self.cardView.layer.cornerRadius = 5.0
        self.cardView.layer.masksToBounds = true
    }

    func set(media: Article.Block.Media) {
        
        //Skeleton
        defaultImage.isHidden = false
        imageContainerView.startShimmering(withIntensity: 0.6)
        
        //Set Thumbnail Image
        var thumbImageURL: URL?
        
        switch media.mediaType {
        case .image:
            thumbImageURL = media.image
            self.playIcon.isHidden = true
        case .video:
            var videoURLString = media.video?.absoluteString
            if videoURLString?.range(of: "youtu") == nil {
                videoURLString = "https://www.youtube.com/watch?v=" + (videoURLString)!
            }
            if let videoID = URLComponents(string: videoURLString!)?.queryItems?.first(where: { $0.name == "v" })?.value {
                thumbImageURL = URL(string: "https://img.youtube.com/vi/\(videoID)/maxresdefault.jpg")
                self.playIcon.isHidden = false
            }
        }
        
        self.imageContainerView?.kf.setImage(with: thumbImageURL, completionHandler: { (_,_,_,_) in
            self.imageContainerView.stopShimmering()
            self.defaultImage.isHidden = true
        })
        
        //Set Description And Tilte
        if media.title == "" && media.description == "" {
            descriptionLabelTopConstraint.constant = 0
            descriptionLabelBottomConstraint.constant = 0
        }
        
        self.descriptionLabel.text = media.description
        self.titleLabel.text = media.title
        
        //Set ColorBar
        colorBar.color = color
        colorBar.id = media.id
    }
}

