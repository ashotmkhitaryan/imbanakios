//
//  NewsArticleTableViewController.swift
//  Imbanak
//
//  Created by Hovak Davtyan on 12/17/17.
//  Copyright © 2017 alfaSolutions. All rights reserved.
//

import UIKit
import Kingfisher
import FacebookShare
import TwitterKit
import FBSDKCoreKit
class NewsArticleTableViewController: UITableViewController {
    //Model Controllers
    var networkController: NetworkController!
    
    //Model
    var headline: Headline!
    var article: Article? {
        didSet {
            configureArticle()
        }
    }
    var relatedNews = [Headline]()
    
    //ArticleBodyTableView
    @IBOutlet var articleBodyTableView: UITableView!
    
    @IBOutlet weak var headerView: UIView!
    
    @IBOutlet weak var headerImageContainerView: UIView!
    @IBOutlet weak var headerImageView: UIImageView!
    @IBOutlet weak var imageViewHeightConstraint: NSLayoutConstraint!
    @IBOutlet weak var headerImagePlaceholder: UIImageView!
    
    @IBOutlet weak var colorBar: ColorBar!
    
    @IBOutlet weak var dateAndSourceContainerView: UIView!
    @IBOutlet weak var hotLineView: UIView!
    @IBOutlet weak var hotLineActionButton: UIButton!
    @IBOutlet weak var facebookButton: UIButton!
    @IBOutlet weak var twitterButton: UIButton!
    @IBOutlet weak var dateAndSourceTopConstraint: NSLayoutConstraint!
    @IBOutlet weak var titleLabelTopConstraint: NSLayoutConstraint!
    @IBOutlet weak var sourceColorBarMain: UIView!
    @IBOutlet weak var sourceColorBarSecondary: UIView!
    
    var headerImageContainerViewConfigured = false

    //Labels Scalable
    private lazy var scaledFont = ScaledFont(fontName: "WeeblySleekUI")
    
    @IBOutlet weak var titleLabel: UILabel! {
        didSet {
            titleLabel.font = scaledFont.font(forTextStyle: .title3)
        }
    }
    @IBOutlet weak var sourceLabel: UILabel! {
        didSet {
            sourceLabel.font = scaledFont.font(forTextStyle: .subheadline)
        }
    }
    @IBOutlet weak var dateLabel: UILabel! {
        didSet {
            dateLabel.font = scaledFont.font(forTextStyle: .subheadline)
        }
    }
    
    @IBOutlet weak var descriptionLabel: UILabel! {
        didSet {
            descriptionLabel.font = scaledFont.font(forTextStyle: .body)
        }
    }
    @IBOutlet weak var descriptionLabelTopConstraint: NSLayoutConstraint!
    
    @IBOutlet weak var hotLineLabel: UILabel! {
        didSet {
            hotLineLabel.font = scaledFont.font(forTextStyle: .footnote)
        }
    }
    
    @IBOutlet weak var relatedNewsTitleLabel: UILabel! {
        didSet {
            relatedNewsTitleLabel.font = scaledFont.font(forTextStyle: .callout)
        }
    }
    
    @IBOutlet weak var footerView: UIView!
    
    
    //RelatedNewsTableView
    @IBOutlet weak var relatedNewsTableView: UITableView!
    @IBOutlet weak var relatedNewsHeaderView: UIView!
    @IBOutlet weak var relatedNewsSeperator: UIView!
    
    //Orientation Properties
    override var shouldAutorotate: Bool {
        return false
    }
    
    override var supportedInterfaceOrientations: UIInterfaceOrientationMask {
        return .portrait
    }
    
    override var preferredInterfaceOrientationForPresentation: UIInterfaceOrientation {
        return .portrait
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        switch headline.source {
        case .mil:
            if headline.thumb == .video {
                colorBar.color = .piRedBrown
            } else {
                colorBar.color = .piDarkBlueGrey
            }
        case .hayzinvor:
            colorBar.color = .piYellowOrange
        case .mandatory, .contract, .tactical:
            colorBar.color = .piDarkSage
        case .service, .product, .staff:
            colorBar.color = .piGunmetal
        }
        
        colorBar.id = headline.id
        
        headerImagePlaceholder.isHidden = false
        headerImageView.startShimmering(withIntensity: 0.4)
        
        networkController.loadArticle(byID: headline.id) { [weak self]
            article in
            guard let article = article else { return }
            DispatchQueue.main.async {
                self?.article = article
            }
        }
        
        //NavigationBar Raw Implementation
        navigationController?.navigationBar.barTintColor = UIColor.piWhite
        navigationController?.navigationBar.setValue(false, forKey: "hidesShadow")
        
        relatedNewsTableView.estimatedRowHeight = 107.0
        relatedNewsTableView.rowHeight = UITableViewAutomaticDimension
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        //Set headerImageView size
        let aspectRatio: CGFloat = 225.0 / 375.0
        let width = view.frame.size.width
        let height = width * aspectRatio
        
        //Set Header Image
        let processor = ResizingImageProcessor(referenceSize: CGSize(width: width, height: height) ,mode: .aspectFill)
        headerImageView.kf.setImage(with: headline.featuredImage, options:[.processor(processor)], completionHandler: { (_,_,_,_) in
            self.headerImagePlaceholder.isHidden = true
            self.headerImageView.stopShimmering()
        })
        
        imageViewHeightConstraint.constant = height
    }
    
    override func viewWillLayoutSubviews() {
        super.viewWillLayoutSubviews()
        
        //Set headerView size
        let headerSize = headerView.systemLayoutSizeFitting(UILayoutFittingExpandedSize)
        if headerView.frame.size.height != headerSize.height {
            headerView.frame.size.height = headerSize.height
            tableView.tableHeaderView = headerView
            tableView.layoutIfNeeded()
        }
        
        let relatedNewsHeaderSize = relatedNewsHeaderView.systemLayoutSizeFitting(UILayoutFittingExpandedSize)
        if relatedNewsHeaderView.frame.size.height != relatedNewsHeaderSize.height {
            relatedNewsHeaderView.frame.size.height = relatedNewsHeaderSize.height
            relatedNewsTableView.tableHeaderView = relatedNewsHeaderView
            relatedNewsTableView.layoutIfNeeded()
        }
        
        //Set footerView size //AssHole Roundtrip
        let aspect = scaledFont.font(forTextStyle: .callout).pointSize / 15
        var footerSize = relatedNewsHeaderSize.height + (CGFloat(relatedNews.count) * 107 * aspect)
        if relatedNews.isEmpty {
            footerSize = 0
        }
        
        if footerView.frame.size.height != footerSize {
            footerView.frame.size.height = footerSize
            tableView.tableFooterView = footerView
            tableView.layoutIfNeeded()
        }
        
        
        //Configure Header Image View
        if !headerImageContainerViewConfigured && headerView.frame.width == headerImageContainerView.frame.width {
            configureHeaderImageView()
        }
    }
    
    func configureHeaderImageView() {
        //Shadows
        self.headerImageContainerView.layer.shadowColor = UIColor.black.cgColor
        self.headerImageContainerView.layer.shadowOpacity = 0.16
        self.headerImageContainerView.layer.shadowOffset = CGSize.zero
        
        let shadow1View = UIView(frame: headerImageContainerView.bounds)
        shadow1View.translatesAutoresizingMaskIntoConstraints = false
        
        shadow1View.layer.shadowColor = UIColor.black.cgColor
        shadow1View.layer.shadowOpacity = 0.16
        shadow1View.layer.shadowOffset = CGSize.zero
        headerImageContainerView.addSubview(shadow1View)
        headerImageContainerView.sendSubview(toBack: shadow1View)
        
        headerImageContainerViewConfigured = true
    }
    
    func configureArticle() {
        guard let article = article else { return }
        titleLabel.text = article.title
        descriptionLabel.text = article.description
        if descriptionLabel.text == "" {
            descriptionLabelTopConstraint.constant = 0
        }
        switch article.source {
        case .mil:
            sourceColorBarMain.backgroundColor = UIColor.piDarkBlueGrey.withAlphaComponent(0.65)
            sourceColorBarSecondary.backgroundColor = UIColor.piDarkBlueGrey
            sourceLabel.text = "www.mil.am"
            facebookButton.setBackgroundImage(UIImage(named: "facebookMil"), for: .normal)
            twitterButton.setBackgroundImage(UIImage(named: "twitterMil"), for: .normal)
            dateLabel.text = headline.publishedAt.stringValue
        case .hayzinvor:
            sourceColorBarMain.backgroundColor = UIColor.piYellowOrange.withAlphaComponent(0.65)
            sourceColorBarSecondary.backgroundColor = UIColor.piYellowOrange
            facebookButton.setBackgroundImage(UIImage(named: "facebookHayzinvor"), for: .normal)
            twitterButton.setBackgroundImage(UIImage(named: "twitterHayzinvor"), for: .normal)
            sourceLabel.text = "www.hayzinvor.am"
            dateLabel.text = headline.publishedAt.stringValue
        default:
            hotLineView.layer.cornerRadius = 2.5
            hotLineView.layer.borderColor = UIColor.piDarkBlueGrey.cgColor
            hotLineView.layer.borderWidth = 1
            hotLineView.layer.masksToBounds = true
            hotLineView.isHidden = false
            hotLineActionButton.isHidden = false
        }
        
        if !article.relatedArticles.isEmpty {
            relatedNews = article.relatedArticles
            relatedNewsTableView.reloadData()
            DispatchQueue.main.async {
                self.relatedNewsTitleLabel.text = "Նմանատիպ Նորություններ"
                self.footerView.backgroundColor = .piWhiteFour
                self.relatedNewsSeperator.backgroundColor = .piWhiteFive
            }
        }

        articleBodyTableView.reloadData()
    }
    
    
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    // MARK: - Table view data source
    
    override func numberOfSections(in tableView: UITableView) -> Int {
        // #warning Incomplete implementation, return the number of sections
        return 1
    }
    
    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if tableView == articleBodyTableView {
            guard let count = article?.blocks.count else { return 0 }
            return count
        } else {
            return relatedNews.count
        }
    }
    
    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        if tableView == articleBodyTableView {
            if let block = article?.blocks[indexPath.row] {
                let articleSource = block.blockType
                let articleContent = block.content
                let articleMedia = block.media
                switch articleSource {
                case .text:
                    let cell = tableView.dequeueReusableCell(withIdentifier: "textCell", for: indexPath) as! NewsArticleTextTableViewCell
                    
                    cell.blockTitleLabel.text = block.title
                    cell.blockSubtitleLabel.text = block.subtitle
                    
                    if block.title == "" && block.subtitle == "" {
                        cell.topConstraint.constant = 0
                        cell.bottomConstraint.constant = 0
                    }
                    
                    guard let text = articleContent else { break }
                    let font = scaledFont.font(forTextStyle: .body)
                    let aux = "<span style=\"font-family: \(font.fontName); font-size: \(font.pointSize)\">\(text)</span>"
                    let htmlData = aux.data(using: .unicode)
                    let attributedString = try! NSMutableAttributedString(data: htmlData!,
                                                                          options: [.documentType: NSAttributedString.DocumentType.html,
                                                                                    .characterEncoding: String.Encoding.unicode.rawValue],
                                                                          documentAttributes: nil)
                    cell.articleTextView.attributedText = attributedString
                    
                    return cell
                case .medias:
                    if articleMedia.count == 1 {
                        let cell = tableView.dequeueReusableCell(withIdentifier: "inlineMediaCell", for: indexPath) as! NewsArticleInlineMediaTableViewCell
                        
                        cell.blockTitleLabel.text = block.title
                        cell.blockSubtitleLabel.text = block.subtitle
                        
                        switch headline.source {
                        case .hayzinvor: cell.color = UIColor.piYellowOrange
                        case .mil: cell.color = UIColor.piDarkBlueGrey
                        default: break
                        }
                        
                        let media = articleMedia.first
                        if media?.mediaType == .video {
                            cell.color = UIColor.piRedBrown
                        }
                        
                        cell.media = media
                        
                        return cell
                    } else {
                        let cell = tableView.dequeueReusableCell(withIdentifier: "galleryCell") as! NewsArticleGalleryTableViewCell
                        cell.medias = articleMedia
                        cell.titleLabel.text = block.title
                        cell.descriptionLabel.text = block.subtitle
                        
                        return cell
                    }
                }
            }
        } else if tableView == relatedNewsTableView {
            let cell = tableView.dequeueReusableCell(withIdentifier: "relatedNewsCell", for: indexPath) as! NewsArticleRelatedNewsTableViewCell
            let relatedHeadline = relatedNews[indexPath.row]
            cell.headline = relatedHeadline
            return cell
        }
        return UITableViewCell() //Investigate this
    }
    
    override func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        if tableView == articleBodyTableView {
            if let block = article?.blocks[indexPath.row] {
                let articleSource = block.blockType
                let articleMedia = block.media
                switch articleSource {
                case .text: return
                case .medias:
                    DispatchQueue.main.async {
                        if let media = articleMedia.first {
                            switch media.mediaType {
                            case .video: self.performSegue(withIdentifier: "InlineVideoSegue", sender: media)
                            case .image: self.performSegue(withIdentifier: "InlinePhotoSegue", sender: media)
                            }
                        }
                    }
                }
            }
        } else if tableView == relatedNewsTableView {
            let relatedHeadline = relatedNews[indexPath.row]
            switch relatedHeadline.thumb {
            case .video:
                if let viewController = self.storyboard?.instantiateViewController(withIdentifier: "YouTubeViewController") as? NewsYoutubePlayerViewController {
                    viewController.headline = relatedHeadline
                    viewController.networkController = networkController
                    self.present(viewController, animated: true, completion: nil)
                }
            case .gallery, .generic:
                if let viewController = self.storyboard?.instantiateViewController(withIdentifier: "NewsArticle") as? NewsArticleTableViewController {
                    viewController.headline = relatedHeadline
                    viewController.networkController = networkController
                    self.navigationController?.pushViewController(viewController, animated: true)
                }
            }
        }
    }
    
    // MARK: - Actions
    @IBAction func hotLineButtonDidTap(_ sender: UIButton) {
        if let url = URL(string: "tel://\("1-28")"), UIApplication.shared.canOpenURL(url) {
            if #available(iOS 10, *) {
                UIApplication.shared.open(url)
            } else {
                UIApplication.shared.openURL(url)
            }
        }
    }
    
    @IBAction func shareTwitterDidTap(_ sender: UIButton) {
        guard let url = article?.fullURL else { return }
        if (UIApplication.shared.canOpenURL(URL(string:"twitter://")!)) {
            if (TWTRTwitter.sharedInstance().sessionStore.hasLoggedInUsers()) {
                // App must have at least one logged-in user to compose a Tweet
                let composer = TWTRComposerViewController.init(initialText: url.absoluteString, image: nil, videoData: nil)
                present(composer, animated: true, completion: nil)
            } else {
                // Log in, and then check again
                TWTRTwitter.sharedInstance().logIn { session, error in
                    if session != nil { // Log in succeeded
                        let composer = TWTRComposerViewController.init(initialText: url.absoluteString, image: nil, videoData: nil)
                        self.present(composer, animated: true, completion: nil)
                    } else {
                        let alert = UIAlertController(title: "No Twitter Accounts Available", message: "You must log in to continue.", preferredStyle: .alert)
                        alert.addAction(UIAlertAction.init(title: "OK", style: .default, handler: nil))
                        self.present(alert, animated: false, completion: nil)
                    }
                }
            }
        } else {
            let alert = UIAlertController(title: "Twitter App Not Found", message: "You must install the Twitter App to continue.", preferredStyle: .alert)
            alert.addAction(UIAlertAction.init(title: "OK", style: .default, handler: nil))
            self.present(alert, animated: false, completion: nil)
        }
    }
    
    @IBAction func shareFacebookDidTap(_ sender: UIButton) {
//        guard let url = article?.fullURL else { return }
//        let shareContent = LinkShareContent(url: URL(url))
//        let shareDialog = ShareDialog(content: shareContent)
//        shareDialog.mode = .automatic
//        shareDialog.presentingViewController = self
//        shareDialog.failsOnInvalidData = true
//        shareDialog.completion = { result in
//            // Handle share results
//        }
//        
//        try? shareDialog.show()
    }
    
   // MARK: - Navigation
    
    @IBAction func imageUnwindToSegue (segue : UIStoryboardSegue) {
        tableView.reloadData()
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        switch segue.identifier! {
        case "InlinePhotoSegue":
            if let destination = segue.destination as? NewsArticleGalleryPageViewController {
                destination.medias = [(sender as! Article.Block.Media)]
                destination.selectedImageIndex = 0
                destination.color = colorBar.color
            }
        case "InlineVideoSegue":
            if let destination = segue.destination as? NewsYoutubePlayerViewController {
                if let media = sender as? Article.Block.Media {
                    destination.videoID = media.video?.absoluteString
                } else if let headline = sender as? Headline {
                    destination.headline = headline
                }
            }
        case "GallerySegue":
            if let destination = segue.destination as? NewsArticleGalleryPageViewController {
                if let senderCell = sender as? NewsArticleGallerCollectionViewCell {
                destination.medias = senderCell.medias
                destination.selectedImageIndex = senderCell.index
                destination.color = colorBar.color
                }
            }
        default: return
        }
    }
    
}
