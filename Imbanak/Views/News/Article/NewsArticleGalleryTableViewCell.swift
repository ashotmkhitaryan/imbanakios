//
//  NewsArticleGalleryTableViewCell.swift
//  Imbanak
//
//  Created by Hovak Davtyan on 12/18/17.
//  Copyright © 2017 alfaSolutions. All rights reserved.
//

import UIKit

class NewsArticleGalleryTableViewCell: UITableViewCell {
    
    @IBOutlet weak var titleLabel: UILabel!
    @IBOutlet weak var descriptionLabel: UILabel!
    
    var medias = [Article.Block.Media]()
}

extension NewsArticleGalleryTableViewCell: UICollectionViewDelegate, UICollectionViewDataSource, UICollectionViewDelegateFlowLayout {
    
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return medias.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "galleryCell", for: indexPath)
        if let galleryCell = cell as? NewsArticleGallerCollectionViewCell {
            if let pathString = medias[indexPath.row].image?.absoluteString.replacingOccurrences(of: "image", with: "image/small") {
                let smallImageUrl = URL(string: pathString)
                galleryCell.defaultImage.isHidden = false
                galleryCell.imageView.startShimmering(withIntensity: 0.92)
                galleryCell.imageView.kf.setImage(with: smallImageUrl, completionHandler: { (_,_,_,_) in
                    galleryCell.imageView?.stopShimmering()
                    galleryCell.defaultImage.isHidden = true
                })
                galleryCell.medias = medias
            }
            galleryCell.index = indexPath.row
        }
        return cell
    }
}
