//
//  WebViewController.swift
//  Imbanak
//
//  Created by Hovak Davtyan on 1/26/18.
//  Copyright © 2018 alfaSolutions. All rights reserved.
//

import UIKit
import WebKit

class WebViewController: UIViewController {
    
    var payementURLString: String!
    @IBOutlet weak var webView: WKWebView!
    @IBOutlet weak var containerView: UIView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        //Setup Container
        containerView.layer.cornerRadius = 5.0
        containerView.layer.masksToBounds = true
        
        if let url = URL(string: payementURLString) {
            let request = URLRequest(url: url)
            webView.load(request)
        }
        // Do any additional setup after loading the view.
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        UIView.animate(withDuration: 0.5) {
            self.view.backgroundColor = UIColor.black.withAlphaComponent(0.2)
        }
        
        //segmentedControlSelectedIndex = 2
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    @IBAction func closeButton(_ sender: UIButton) {
        view.backgroundColor = UIColor.black.withAlphaComponent(0.0)
        
        dismiss(animated: true, completion: nil)
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
