//
//  ThausandPlusViewController.swift
//  Imbanak
//
//  Created by Hovak Davtyan on 12/30/17.
//  Copyright © 2017 alfaSolutions. All rights reserved.
//

import UIKit
import SWSegmentedControl

class ThausandPlusViewController: UIViewController {

    
    @IBOutlet weak var segmentedControlContainerView: UIView!
    @IBOutlet weak var donatedTitleLabel: UILabel!
    @IBOutlet weak var donatedLabel: UILabel!
    @IBOutlet weak var payedTitleLabel: UILabel!
    @IBOutlet weak var payedLabel: UILabel!
    @IBOutlet weak var donateButton: UIButton!
    @IBOutlet weak var officialPageButton: UIButton!
    @IBOutlet weak var curencyLabel: UILabel!
    @IBOutlet weak var currencyLabelCompensations: UILabel!

    private var token: String! {
        didSet {
            DispatchQueue.main.async {
                self.donateButton.isEnabled = true
                self.donateButton.backgroundColor = UIColor.piYellowOrange
            }
        }
    }
    private var currencyRates: CurrencyRates!
    
    private var counters: ThausandPlusCounters! {
        didSet {
            let totalDonated = String((Int(round(counters.data.mandatorySum + counters.data.freewillSum))))
            let totalCompensation = String((Int(round(counters.data.compensationSum))))
            let donatedDigitCount = 15 - totalDonated.count
            let compensationDigitCount = 15 - totalCompensation.count
            let donated = String(repeating: "0", count: donatedDigitCount) + totalDonated
            let compensation = String(repeating: "0", count: compensationDigitCount) + totalCompensation
            
            let donatedAttributedString = NSMutableAttributedString(string: donated)
            donatedAttributedString.addAttribute(.foregroundColor, value: UIColor.piWhiteSix, range: NSRange(location: 0, length: donatedDigitCount))
            
            let payedAttributedString = NSMutableAttributedString(string: compensation)
            payedAttributedString.addAttribute(.foregroundColor, value: UIColor.piWhiteSix, range: NSRange(location: 0, length: compensationDigitCount))
            
            DispatchQueue.main.async {
                self.donatedLabel.attributedText = donatedAttributedString
                self.payedLabel.attributedText = payedAttributedString
            }
        }
    }
    
    var segmentedControl: SWSegmentedControl!
    
    override var shouldAutorotate: Bool {
        return false
    }
    
    override var supportedInterfaceOrientations: UIInterfaceOrientationMask {
        return .portrait
    }
    
    override var preferredInterfaceOrientationForPresentation: UIInterfaceOrientation {
        return .portrait
    }
    
    
    @IBAction func officialPage(_ sender: UIButton) {
        print("Triggered")
        if let url = URL(string: "https://www.1000plus.am") {
            UIApplication.shared.open(url, options: [:])
        }
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        segmentedControl = SWSegmentedControl(items: ["֏", "₽", "＄", "€"])
        segmentedControl.indicatorColor = UIColor.piDarkBlueGrey
        segmentedControl.indicatorThickness = 3.0
        segmentedControl.font = UIFont(name: "WeblySleekUISemibold", size: 24)!
        segmentedControl.titleColor = UIColor.black
        segmentedControl.unselectedTitleColor = UIColor.black
        segmentedControl.delegate = self
        
        let zeros = NSMutableAttributedString(string: "000000000000000")
        zeros.addAttribute(.foregroundColor, value: UIColor.piWhiteSix, range: NSRange(location: 0, length: 15))
        donatedLabel.attributedText = zeros
        payedLabel.attributedText = zeros
        
        //Requests
        getToken()
        getCounters()
        getCurrencyRates()
        
        officialPageButton.layer.cornerRadius = 2.5
        officialPageButton.layer.borderWidth = 2
        officialPageButton.layer.borderColor = UIColor.piDarkBlueGrey.cgColor
        officialPageButton.layer.masksToBounds = true
        donateButton.layer.cornerRadius = 2.5
        donateButton.layer.masksToBounds = true
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        if !donateButton.isEnabled {
            donateButton.backgroundColor = UIColor.piYellowOrange.withAlphaComponent(0.3)
        }
    }
    
    override func viewDidLayoutSubviews() {
        segmentedControl.frame = CGRect(x: 0, y: 0,
                                        width: segmentedControlContainerView.frame.size.width,
                                        height: segmentedControlContainerView.frame.height)
        
        segmentedControlContainerView.addSubview(segmentedControl)
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    // Mark: - Request Methods
    
    func getToken() {
        //Get Token
        let baseURL = "https://www.1000plus.am/hy/api/csrf-token?deviceType=iPhone"
        let convertedString = baseURL.addingPercentEncoding(withAllowedCharacters: .urlFragmentAllowed)
        guard let tokenURL = URL(string: convertedString!) else {
            print("wrongURL")
            return
        }
        let userAgent = "5350865897949205163446608"
        var request = URLRequest.init(url: tokenURL)
        request.setValue(userAgent, forHTTPHeaderField: "User-Agent")
        let session = URLSession.shared
        let task = session.dataTask(with: request, completionHandler: {
            (data, response, error) in
            guard error == nil else {
                //Handle error
                print(error ?? "Unknown Error Accured")
                return
            }
            
            guard let responseData = data else {
                print("Error: did not receive data")
                return
            }
            
            do {
                if let tokenJSON = try JSONSerialization.jsonObject(with: responseData, options: []) as? [String: String],
                let token = tokenJSON["data"], let status = tokenJSON["status"] {
                    if status == "OK" {
                        print("TokenOK")
                        self.token = token
                    }
                    // created Token
                } else {
                    // couldn't create a NewsFeed object from the JSON
                    print("Couldn't create a token object from the JSON")
                }
            } catch {
                // error trying to convert the data to JSON using JSONSerialization.jsonObject
                print("Respone: \(responseData)")
                print("cannot Convert data through serialization")
                return
            }
        })
        
        task.resume()
        
    }
    
    func getCounters() {
        //Get Token
        let baseURL = "https://www.1000plus.am/hy/api/counters?deviceType=iPhone"
        let convertedString = baseURL.addingPercentEncoding(withAllowedCharacters: .urlFragmentAllowed)
        guard let countersURL = URL(string: convertedString!) else {
            print("wrongURL")
            return
        }
        let userAgent = "5350865897949205163446608"
        var request = URLRequest.init(url: countersURL)
        request.setValue(userAgent, forHTTPHeaderField: "User-Agent")
        let session = URLSession.shared
        let task = session.dataTask(with: request, completionHandler: {
            (data, response, error) in
            guard error == nil else {
                //Handle error
                print(error ?? "Unknown Error Accured")
                return
            }
            
            guard let responseData = data else {
                print("Error: did not receive data")
                return
            }
            
            do {
                if let countersJSON = try JSONSerialization.jsonObject(with: responseData, options: []) as? [String: Any],
                    let counters = ThausandPlusCounters(json: countersJSON) {
                    if counters.status == "OK" {
                        print("CountersOK")
                        self.counters = counters
                    }
                    // created Token
                } else {
                    // couldn't create a NewsFeed object from the JSON
                    print("Couldn't create a counters object from the JSON")
                }
            } catch {
                // error trying to convert the data to JSON using JSONSerialization.jsonObject
                print("cannot Convert data through serialization")
                return
            }
        })
        
        task.resume()
        
    }
    
    func getCurrencyRates() {
        //Get Token
        let baseURL = "https://www.1000plus.am/ru/api/rates"
        let convertedString = baseURL.addingPercentEncoding(withAllowedCharacters: .urlFragmentAllowed)
        guard let countersURL = URL(string: convertedString!) else {
            print("wrongURL")
            return
        }
        let userAgent = "5350865897949205163446608"
        var request = URLRequest.init(url: countersURL)
        request.setValue(userAgent, forHTTPHeaderField: "User-Agent")
        let session = URLSession.shared
        let task = session.dataTask(with: request, completionHandler: {
            (data, response, error) in
            guard error == nil else {
                //Handle error
                print(error ?? "Unknown Error Accured")
                return
            }
            
            guard let responseData = data else {
                print("Error: did not receive data")
                return
            }
            
            do {
                if let currencyRatesJSON = try JSONSerialization.jsonObject(with: responseData, options: []) as? [String: Any],
                    let currencyRates = CurrencyRates(json: currencyRatesJSON) {
                    if currencyRates.status == "OK" {
                        print("CurrencyRatesOK")
                        self.currencyRates = currencyRates
                    }
                    // created Token
                } else {
                    // couldn't create a NewsFeed object from the JSON
                    print("Couldn't create a currencyRates object from the JSON")
                }
            } catch {
                // error trying to convert the data to JSON using JSONSerialization.jsonObject
                print("cannot Convert data through serialization")
                return
            }
        })
        
        task.resume()
        
    }
    
    


    // MARK: - Navigation

    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if let destination = segue.destination as? ThausandPlusDetailsViewController {
            destination.segmentedControlSelectedIndex = segmentedControl.selectedSegmentIndex
            destination.token = self.token
            print("passingDate")
        }
    }
    
    @IBAction func unwindToSegue (segue : UIStoryboardSegue) {
        var currencyIndex = 0
        
        if let source = segue.source as? ThausandPlusDetailsViewController {
            currencyIndex = source.segmentedControlSelectedIndex
        }
        
        if let destination = segue.destination as? ThausandPlusViewController {
            destination.segmentedControl.setSelectedSegmentIndex(currencyIndex)
        }
    }

}

extension ThausandPlusViewController: SWSegmentedControlDelegate {
    func segmentedControl(_ control: SWSegmentedControl, canSelectItemAtIndex index: Int) -> Bool {
        switch index {
        case 0: return true
        case 1: return currencyRates?.data.rub != nil ? true : false
        case 2: return currencyRates?.data.usd != nil ? true : false
        case 3: return currencyRates?.data.eur != nil ? true : false
        default: return false
        }
    }
    
    func segmentedControl(_ control: SWSegmentedControl, willSelectItemAtIndex index: Int) {
        var currencyAdjustedDonated: Double = 0
        var currencyAdjustedCompensation: Double = 0

        switch index {
        case 0:
            currencyLabelCompensations.text = "֏"
            curencyLabel.text = "֏"
            currencyAdjustedDonated = (counters.data.mandatorySum + counters.data.freewillSum)
            currencyAdjustedCompensation = counters.data.compensationSum
        case 1:
            currencyLabelCompensations.text = "₽"
            curencyLabel.text = "₽"
            currencyAdjustedDonated = (counters.data.mandatorySum + counters.data.freewillSum) / (currencyRates?.data.rub)!
            currencyAdjustedCompensation = counters.data.compensationSum / (currencyRates?.data.rub)!
        case 2:
            currencyLabelCompensations.text = "＄"
            curencyLabel.text = "＄"
            currencyAdjustedDonated = (counters.data.mandatorySum + counters.data.freewillSum) / (currencyRates?.data.usd)!
            currencyAdjustedCompensation = counters.data.compensationSum / (currencyRates?.data.usd)!
        case 3:
            currencyLabelCompensations.text = "€"
            curencyLabel.text = "€"
            currencyAdjustedDonated = (counters.data.mandatorySum + counters.data.freewillSum) / (currencyRates?.data.eur)!
            currencyAdjustedCompensation = counters.data.compensationSum / (currencyRates?.data.eur)!
        default: break
        }
        
        let totalDonated = String((Int(round(currencyAdjustedDonated))))
        let totalCompensation = String((Int(round(currencyAdjustedCompensation))))
        let donatedDigitCount = 15 - totalDonated.count
        let compensationDigitCount = 15 - totalCompensation.count
        let donated = String(repeating: "0", count: donatedDigitCount) + totalDonated
        let compensation = String(repeating: "0", count: compensationDigitCount) + totalCompensation
        
        let donatedAttributedString = NSMutableAttributedString(string: donated)
        donatedAttributedString.addAttribute(.foregroundColor, value: UIColor.piWhiteSix, range: NSRange(location: 0, length: donatedDigitCount))
        
        let payedAttributedString = NSMutableAttributedString(string: compensation)
        payedAttributedString.addAttribute(.foregroundColor, value: UIColor.piWhiteSix, range: NSRange(location: 0, length: compensationDigitCount))
        
        self.donatedLabel.attributedText = donatedAttributedString
        self.payedLabel.attributedText = payedAttributedString
    }
}
