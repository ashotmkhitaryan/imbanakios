//
//  ThausandPlusDetailsViewController.swift
//  Imbanak
//
//  Created by Hovak Davtyan on 12/31/17.
//  Copyright © 2017 alfaSolutions. All rights reserved.
//

import UIKit
import SWSegmentedControl

class ThausandPlusDetailsViewController: UIViewController {
    
    @IBOutlet weak var containerView: UIScrollView!
    @IBOutlet weak var scrollView: UIScrollView!
    @IBOutlet weak var segmentedControlContainerView: UIView!
    @IBOutlet weak var otherContainerHeightConstraint: NSLayoutConstraint!
    @IBOutlet weak var otherContainer: UIView!
    
    @IBOutlet weak var amountTextField: UITextField!
    @IBOutlet weak var otherAmountTextField: UITextField!
    @IBOutlet weak var nameTextField: UITextField!
    @IBOutlet weak var surnameTextFied: UITextField!
    @IBOutlet weak var emailTextField: UITextField!
    @IBOutlet weak var keepSecretSwitch: UISwitch!
    @IBOutlet weak var periodicalPaymentsSwitch: UISwitch!
    
    @IBOutlet weak var donateButton: UIButton!
    
    var token: String!
    var segmentedControl: SWSegmentedControl!
    var segmentedControlSelectedIndex: Int!
    var pickerViewSelectedRow = 0
    
    //Picker
    let pickerViewData = [
        "051" : ["5000", "10000", "20000", "50000", "100000", "Այլ"],
        "643" : ["1000", "2000", "5000", "10000", "50000", "Այլ"],
        "840" : ["10", "20", "50", "100", "500", "Այլ"],
        "978" : ["10", "20", "50", "100", "500", "Այլ"]
    ]
    lazy var pickerView = UIPickerView()
    
    //Keyboard Accesory Input View
    var accessoryToolbar: UIToolbar!
    var lastTextField: UITextField!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        //Setup Container
        containerView.layer.cornerRadius = 5.0
        containerView.layer.masksToBounds = true
        
        //Hide otherContainer
        otherContainer.isHidden = true
        otherContainerHeightConstraint.constant = 0
        
        //Keyboard Adjustments
        let notificationCenter = NotificationCenter.default
        notificationCenter.addObserver(self, selector: #selector(adjustForKeyboard), name: Notification.Name.UIKeyboardWillHide, object: nil)
        notificationCenter.addObserver(self, selector: #selector(adjustForKeyboard), name: Notification.Name.UIKeyboardWillChangeFrame, object: nil)
        
        //Setup PickerView
        pickerView.delegate = self
        pickerView.dataSource = self
        pickerView.selectRow(0, inComponent: 0, animated: false)
        
        var currencyKey = ""
        switch segmentedControlSelectedIndex {
        case 0: currencyKey = "051"
        case 1: currencyKey = "643"
        case 2: currencyKey = "840"
        case 3: currencyKey = "978"
        default: break
        }
        
        amountTextField.text = pickerViewData[currencyKey]?.first
        amountTextField.inputView = pickerView
        
        
        segmentedControl = SWSegmentedControl(items: ["֏", "₽", "＄", "€"])
        segmentedControl.delegate = self
        segmentedControl.indicatorColor = UIColor.piDarkBlueGrey
        segmentedControl.indicatorThickness = 3.0
        segmentedControl.font = UIFont(name: "WeblySleekUISemibold", size: 24)!
        segmentedControl.titleColor = UIColor.black
        segmentedControl.unselectedTitleColor = UIColor.black
        segmentedControl.setSelectedSegmentIndex(segmentedControlSelectedIndex)
        
        donateButton.layer.cornerRadius = 2.5
        donateButton.layer.masksToBounds = true
        // Do any additional setup after loading the view.
    }
    
    override func viewDidLayoutSubviews() {
        
        segmentedControl.frame = CGRect(x: 0, y: 0,
                                        width: segmentedControlContainerView.frame.size.width,
                                        height: segmentedControlContainerView.frame.height)
        
        segmentedControlContainerView.addSubview(segmentedControl)
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        UIView.animate(withDuration: 0.5) {
            self.view.backgroundColor = UIColor.black.withAlphaComponent(0.2)
        }
        
        //segmentedControlSelectedIndex = 2
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    // MARK: - Actions
    @IBAction func donateButton(_ sender: UIButton) {
        
        self.view.endEditing(true)
        
        var data = [String : String]()
        var dataIsReady = true
        
        //Define Currency
        var currencyKey = ""
        switch segmentedControlSelectedIndex {
        case 0: currencyKey = "051"
        case 1: currencyKey = "643"
        case 2: currencyKey = "840"
        case 3: currencyKey = "978"
        default: break
        }
        
        //Currency
        if currencyKey != "" {
            data["currency"] = currencyKey
        } else {
            dataIsReady = false
            print("wrongCurrency")
        }
        
        //stampID or Amount
        if pickerView.selectedRow(inComponent: 0) != 5 {
            var stampID = pickerView.selectedRow(inComponent: 0) + 1
            
            switch currencyKey {
            case "643":
                stampID += 5
            case "840":
                stampID += 10
            case "978":
                stampID += 15
            default: break
            }
            
            data["stamp_id"] = String(stampID)
            
        } else {
            if let amount = otherAmountTextField.text {
                if isStringAnInt(string: amount) {
                    data["amount"] = amount
                } else {
                    dataIsReady = false
                    //Notify Wrong Amount
                }
            }
        }
        
        //FirstName
        if let firstName = nameTextField.text {
            if !firstName.isEmpty {
                data["first_name"] = trimEndWhiteSpace(string: firstName)
            } else {
                dataIsReady = false
                nameTextField.textColor = UIColor.red
                nameTextField.text = "Պարտադիր դաշտ լրացման համար"
            }
        } else {
            dataIsReady = false
            print("firstnameIsNil")
        }
        
        //LastName
        if let lastName = surnameTextFied.text {
            if !lastName.isEmpty {
                data["last_name"] = trimEndWhiteSpace(string: lastName)
            } else {
                dataIsReady = false
                surnameTextFied.textColor = UIColor.red
                surnameTextFied.text = "Պարտադիր դաշտ լրացման համար"
            }
        } else {
            dataIsReady = false
            print("lastnameIsNil")
        }
        
        //Email
        if var email = emailTextField.text {
            email = trimEndWhiteSpace(string: email)
            if !email.isEmpty {
                if isValidEmail(testStr: email) {
                    data["email"] = email
                } else {
                    dataIsReady = false
                    emailTextField.textColor = UIColor.red
                    emailTextField.text = "Սխալ Էլ.փոստ"
                }
            } else {
                dataIsReady = false
                emailTextField.textColor = UIColor.red
                emailTextField.text = "Պարտադիր դաշտ լրացման համար"
            }
        } else {
            dataIsReady = false
            print("emailIsNil")
        }
        
        //Anonymous
        switch keepSecretSwitch.isOn {
        case true: data["is_anonymous"] = "1"
        case false: data["is_anonymous"] = "0"
        }
        
        //Monthly Payments
        switch periodicalPaymentsSwitch.isOn {
        case true: data["monthly_fee"] = "1"
        case false: data["monthly_fee"] = "0"
        }
        
        print("FinalData: \(data)")
        print("Data Is Ready \(dataIsReady)")
        
        if dataIsReady {
            pay(data: data, token: token)
        }
    }
    
    @IBAction func closeButton(_ sender: UIButton) {
        view.backgroundColor = UIColor.black.withAlphaComponent(0.0)
        
        dismiss(animated: true, completion: nil)
    }
    
    @IBAction func detailUnwindToSegue (segue : UIStoryboardSegue) {
        view.backgroundColor = UIColor.black.withAlphaComponent(0.0)
        
        dismiss(animated: true, completion: nil)
    }
    
    func isValidEmail(testStr:String) -> Bool {
        //Check Expression
        let emailRegEx = "[A-Z0-9a-z._%+-]+@[A-Za-z0-9.-]+\\.[A-Za-z]{2,64}"
        
        let emailTest = NSPredicate(format:"SELF MATCHES %@", emailRegEx)
        return emailTest.evaluate(with: testStr)
    }
    
    func trimEndWhiteSpace (string: String) -> String {
        return string.replacingOccurrences(of: "\\s+$", with: "", options: .regularExpression)
    }
    
    func isStringAnInt(string: String) -> Bool {
        return Int(string) != nil
    }
    
    // MARK: - Selector
    
    @objc func adjustForKeyboard(notification: Notification) {
        let userInfo = notification.userInfo!
        
        let keyboardScreenEndFrame = (userInfo[UIKeyboardFrameEndUserInfoKey] as! NSValue).cgRectValue
        let keyboardViewEndFrame = view.convert(keyboardScreenEndFrame, from: view.window)
        
        if notification.name == Notification.Name.UIKeyboardWillHide {
            scrollView.contentInset = UIEdgeInsets.zero
        } else {
            scrollView.contentInset = UIEdgeInsets(top: 0, left: 0, bottom: keyboardViewEndFrame.height + accessoryToolbar.frame.height / 2, right: 0)
        }
        
        scrollView.scrollIndicatorInsets = scrollView.contentInset
    }
    
    //Request Methods
    func pay(data: [String : String], token: String) {
        
        //Prepare Data
        let data: [String : Any] = [
            "_token" : token ,
            "data" : data
        ]
        
        //Prepare URL
        let baseURL = "https://www.1000plus.am/hy/payment/pay?deviceType=iPhone"
        let convertedString = baseURL.addingPercentEncoding(withAllowedCharacters: .urlFragmentAllowed)
        guard let payementURL = URL(string: convertedString!) else {
            print("wrongURL")
            return
        }
        let userAgent = "5350865897949205163446608"
        var request = URLRequest.init(url: payementURL)
        request.httpMethod = "POST"
        request.setValue(userAgent, forHTTPHeaderField: "User-Agent")
        request.addValue("application/json", forHTTPHeaderField: "Content-Type")
        
        guard let httpBody = try? JSONSerialization.data(withJSONObject: data, options: []) else {
            print("Serialization Error")
            return
        }
        
        request.httpBody = httpBody
  
        let session = URLSession.shared
        let task = session.dataTask(with: request, completionHandler: {
            (data, response, error) in
            guard error == nil else {
                //Handle error
                print(error ?? "Unknown Error Accured")
                return
            }
            
            guard let responseData = data else {
                print("Error: did not receive data")
                return
            }
            
            do {
                if let responseJSON = try JSONSerialization.jsonObject(with: responseData, options: []) as? [String: Any],
                let status = responseJSON["status"] as? String, let data = responseJSON["data"] as? [String : String] {
                    if status == "OK" {
                        if let urlString = data["redirect_uri"] {
                            if let url = URL(string: urlString) {
                                DispatchQueue.main.async {
                                    UIApplication.shared.open(url, options: [:])
                                    self.dismiss(animated: false, completion: nil)
                                }
                            }
                            //self.performSegue(withIdentifier: "webSegue", sender: url)
                        }
                    }
                    // created Token
                } else {
                    // couldn't create a NewsFeed object from the JSON
                    print("Couldn't create a token object from the JSON")
                }
            } catch {
                // error trying to convert the data to JSON using JSONSerialization.jsonObject
                print("Respone: \(responseData)")
                print("cannot Convert data through serialization")
                return
            }
        })
        
        task.resume()
        
    }
    
    
    

     // MARK: - Navigation
     override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == "webSegue" {
            print("WILLSEGUE")
            if let destination = segue.destination as? WebViewController {
                destination.payementURLString = sender as! String
            }
        }
     }

    
}

extension ThausandPlusDetailsViewController: UIPickerViewDelegate, UIPickerViewDataSource {
    func numberOfComponents(in pickerView: UIPickerView) -> Int {
        return 1
    }
    
    func pickerView(_ pickerView: UIPickerView, numberOfRowsInComponent component: Int) -> Int {
        return (pickerViewData["051"]?.count)!
    }
    
    func pickerView(_ pickerView: UIPickerView, titleForRow row: Int, forComponent component: Int) -> String? {
        let index = segmentedControlSelectedIndex!
        var currencyKey = ""
        
        switch index {
        case 0: currencyKey = "051"
        case 1: currencyKey = "643"
        case 2: currencyKey = "840"
        case 3: currencyKey = "978"
        default: break
        }
        
        return pickerViewData[currencyKey]?[row]
    }
    
    func pickerView(_ pickerView: UIPickerView, didSelectRow row: Int, inComponent component: Int) {
        let index = segmentedControl.selectedSegmentIndex
        pickerViewSelectedRow = row
        var currencyKey = ""
        
        switch index {
        case 0: currencyKey = "051"
        case 1: currencyKey = "643"
        case 2: currencyKey = "840"
        case 3: currencyKey = "978"
        default: break
        }
        
        amountTextField.text = pickerViewData[currencyKey]?[row]
        
        if row == 5 {
            otherContainerHeightConstraint.constant = 40
            otherContainer.isHidden = false
            view.viewWithTag(2)?.becomeFirstResponder()
        } else {
            otherContainer.isHidden = true
            otherContainerHeightConstraint.constant = 0
        }
    }
}

extension ThausandPlusDetailsViewController: UITextFieldDelegate {
    
    func textFieldShouldBeginEditing(_ textField: UITextField) -> Bool {
        if textField.textColor == UIColor.red {
            textField.text = ""
            textField.textColor = UIColor.black
        }
        // if there's no tool bar, create it
        if accessoryToolbar == nil {
            accessoryToolbar = UIToolbar.init(frame: CGRect(x: 0, y: 0, width: self.view.frame.width, height: 44))
            let previousBarButtonItem = UIBarButtonItem(image: UIImage(named: "previousBarButtonItem"), style: .plain, target: self, action: #selector(previousButtonTapped))
            let fixedSpaceBarButtonItem = UIBarButtonItem(barButtonSystemItem: .fixedSpace, target: self, action: nil)
            fixedSpaceBarButtonItem.width = 20
            let nextBarButtonItem = UIBarButtonItem(image: UIImage(named: "nextBarButtonItem"), style: .plain, target: self, action: #selector(nextButtonTapped))
            let flexibleSpaceBarButtonIntem = UIBarButtonItem(barButtonSystemItem: .flexibleSpace, target: self, action: nil)
            let doneBarButtonItem = UIBarButtonItem(barButtonSystemItem: .done, target: self, action: #selector(doneButtonTapped))
            accessoryToolbar.items = [previousBarButtonItem, fixedSpaceBarButtonItem, nextBarButtonItem, flexibleSpaceBarButtonIntem, doneBarButtonItem]
        }
        //Disable Buttonas for the first and last textFields
        if textField.tag == 1 {
            accessoryToolbar.items?[0].isEnabled = false
        } else {
            accessoryToolbar.items?[0].isEnabled = true
        }
        
        if textField.tag == 5 {
            accessoryToolbar.items?[2].isEnabled = false
        } else {
            accessoryToolbar.items?[2].isEnabled = true
        }
        
        
        textField.inputAccessoryView = accessoryToolbar
        lastTextField = textField
        return true
    }
    
    @objc func previousButtonTapped() {
        if let previousField = view.viewWithTag(lastTextField.tag - 1) as? UITextField {
            if let containerView = previousField.superview {
                if !containerView.isHidden {
                    previousField.becomeFirstResponder()
                } else {
                    if let previousField = view.viewWithTag(lastTextField.tag - 2) as? UITextField {
                        previousField.becomeFirstResponder()
                    }
                }
            }
        }
    }
    
    @objc func nextButtonTapped() {
        if let nextField = view.viewWithTag(lastTextField.tag + 1) as? UITextField {
            if let containerView = nextField.superview {
                if !containerView.isHidden {
                    nextField.becomeFirstResponder()
                } else {
                    if let nextField = view.viewWithTag(lastTextField.tag + 2) as? UITextField {
                        nextField.becomeFirstResponder()
                    }
                }
            }
        }
    }
    
    @objc func doneButtonTapped() {
        lastTextField.resignFirstResponder()
    }
    
}

extension ThausandPlusDetailsViewController: SWSegmentedControlDelegate {
    func segmentedControl(_ control: SWSegmentedControl, willSelectItemAtIndex index: Int) {
        segmentedControlSelectedIndex = index
        var currencyKey = ""
        
        print(index)
        
        switch index {
        case 0: currencyKey = "051"
        case 1: currencyKey = "643"
        case 2: currencyKey = "840"
        case 3: currencyKey = "978"
        default: break
        }
        
        amountTextField.text = pickerViewData[currencyKey]?[pickerViewSelectedRow]
        pickerView.reloadAllComponents()
    }
}
