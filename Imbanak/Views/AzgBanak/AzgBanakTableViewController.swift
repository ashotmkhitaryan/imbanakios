//
//  MilServiceTableViewController.swift
//  Imbanak
//
//  Created by Hovak Davtyan on 12/28/17.
//  Copyright © 2017 alfaSolutions. All rights reserved.
//

import UIKit

class AzgBanakTableViewController: UITableViewController {
    
    var networkController: NetworkController!
    
    //Orientation Properties
    override var shouldAutorotate: Bool {
        return false
    }
    
    override var supportedInterfaceOrientations: UIInterfaceOrientationMask {
        return .portrait
    }
    
    override var preferredInterfaceOrientationForPresentation: UIInterfaceOrientation {
        return .portrait
    }
    
    var headlines = [Headline]()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        tableView.estimatedRowHeight = 108
        tableView.rowHeight = UITableViewAutomaticDimension
        self.title = "Ազգ-բանակ"
        
        navigationController?.navigationBar.prefersLargeTitles = true
        navigationController?.navigationBar.tintColor = UIColor.piDarkBlueGrey
        
        networkController.loadHeadlines(from: 0, to: 9999, ofType: .milServices) { [weak self]
            headlines in
            guard let headlines = headlines else { return }
            self?.headlines = headlines.filter{ $0.source == .contract }
            self?.tableView.reloadData()
        }
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        if let navigationController = self.navigationController as? IBNavigationController {
            navigationController.switchStyle()
        }
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    //MARK: TableView DataSource
    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return headlines.count
    }
    
    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "AzgBanakCell", for: indexPath)
        if let azgBanakCell = cell as? AzgBanakTableViewCell {
            azgBanakCell.headline = headlines[indexPath.row]
        }
        return cell
    }
    
    //MARK: TableView Delegate
    override func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
            let headline = headlines[indexPath.row]
            performSegue(withIdentifier: "ArticleSegue", sender: headline)

    }
    
    override func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        let maximumSize = tableView.estimatedRowHeight
        let scaledFont = ScaledFont.init(fontName: "WeeblySleekUI")
        let aspect = scaledFont.font(forTextStyle: .title3).pointSize / 20
        return maximumSize * aspect
    }
    
    override func scrollViewDidScroll(_ scrollView: UIScrollView) {
        if let navigationController = self.navigationController as? IBNavigationController {
            navigationController.switchStyle()
        }
    }
    
    
    //MARK: - Navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == "ArticleSegue" {
            if let destination = segue.destination as? NewsArticleTableViewController {
                destination.headline = sender as? Headline
                destination.networkController = networkController
            }
        }
    }
}
