//
//  AzgBanakTableViewCell.swift
//  Imbanak
//
//  Created by Hovak Davtyan on 4/10/18.
//  Copyright © 2018 alfaSolutions. All rights reserved.
//

import UIKit

class AzgBanakTableViewCell: UITableViewCell {

    @IBOutlet weak var cardView: UIView!
    @IBOutlet weak var colorBar: ColorBar!
    
    @IBOutlet weak var featuredImageView: UIImageView!
    @IBOutlet weak var defaultImageView: UIImageView!
    @IBOutlet weak var titleLabel: UILabel! {
        didSet {
            titleLabel.font = scaledFont.font(forTextStyle: .title3)
        }
    }
    
    var headline: Headline! {
        didSet {
            setHeadline(headline)
        }
    }
    
    let cardInset: CGFloat = 16
    
    var cardConfigured = false
    
    private lazy var scaledFont = ScaledFont(fontName: "WeeblySleekUI")
    
    override func awakeFromNib() {
        super.awakeFromNib()
        
        self.cardView.layer.cornerRadius = 5.0
        self.cardView.layer.masksToBounds = true
    }
    
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
    }
    
    private func setHeadline(_ headline: Headline) {

        
        //Title
        titleLabel.text = headline.title
        
        //Image
        let imageURL = headline.featuredImage
        featuredImageView.startShimmering(withIntensity: 0.6)
        featuredImageView.kf.setImage(with: imageURL) { [weak self]
            (image, error, cache, url) in
            self?.defaultImageView.isHidden = true
            self?.featuredImageView.stopShimmering()
        }
        
        //ColorBar
        switch headline.source {
        case .mil:
            if headline.thumb == .video {
                colorBar.color = .piRedBrown
            } else {
                colorBar.color = .piDarkBlueGrey
            }
        case .hayzinvor:
            colorBar.color = .piYellowOrange
        case .mandatory, .contract, .tactical:
            colorBar.color = .piDarkSage
        case .service, .product, .staff:
            colorBar.color = .piGunmetal
        }
        colorBar.id = headline.id
    }
}
