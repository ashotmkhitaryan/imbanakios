//
//  AppDelegate.swift
//  Imbanak
//
//  Created by Hovak Davtyan on 11/28/17.
//  Copyright © 2017 alfaSolutions. All rights reserved.
//

import UIKit
import GoogleMaps
import YoutubeKit
import TwitterKit
import Kingfisher

@UIApplicationMain
class AppDelegate: UIResponder, UIApplicationDelegate {

    var window: UIWindow?
    
    func application(_ app: UIApplication, open url: URL, options: [UIApplicationOpenURLOptionsKey : Any] = [:]) -> Bool {
        return TWTRTwitter.sharedInstance().application(app, open: url, options: options)
    }

    func application(_ application: UIApplication, didFinishLaunchingWithOptions launchOptions: [UIApplicationLaunchOptionsKey: Any]?) -> Bool {
        
        ImageCache.default.maxMemoryCost = 1

        if let tabBarController = window?.rootViewController as? UITabBarController {
            let networkController = NetworkController()
            let tabBarTextAttributes = [NSAttributedStringKey.foregroundColor : UIColor.clear]
            if let tabBarItems = tabBarController.tabBar.items {
                for item in tabBarItems {
                    item.setTitleTextAttributes(tabBarTextAttributes, for: .normal)
                    item.setTitleTextAttributes(tabBarTextAttributes, for: .highlighted)
                    item.setTitleTextAttributes(tabBarTextAttributes, for: .selected)
                }
            }
            
            for viewController in tabBarController.viewControllers! {
                if let navigationController = viewController as? IBNavigationController {
                    if let rootViewController = navigationController.viewControllers.first as? NewsFeedTableViewController {
                        rootViewController.networkController = networkController
                    }
                    if let rootViewController = navigationController.viewControllers.first as? AzgBanakTableViewController {
                        rootViewController.networkController = networkController
                    }
                    if let rootViewController = navigationController.viewControllers.first as? EducationTableViewController {
                        rootViewController.networkController = networkController
                    }
                }
            }
        }
        
        GMSServices.provideAPIKey("AIzaSyAMGDr6MIMmmTAdvzEyJC5rKgsxz8LrdoM")
        YoutubeKit.shared.setAPIKey("AIzaSyAMGDr6MIMmmTAdvzEyJC5rKgsxz8LrdoM")
        
        TWTRTwitter.sharedInstance().start(withConsumerKey:"5O10OXPDA4fQRNEv7ZrkDrkX4", consumerSecret:"mhsqP18jSn6NwrcyMK2IqiuPuXTookteDsPZFeOZuKSxGrSnDI")
        return true
    }

    func applicationWillResignActive(_ application: UIApplication) {
        // Sent when the application is about to move from active to inactive state. This can occur for certain types of temporary interruptions (such as an incoming phone call or SMS message) or when the user quits the application and it begins the transition to the background state.
        // Use this method to pause ongoing tasks, disable timers, and invalidate graphics rendering callbacks. Games should use this method to pause the game.
    }

    func applicationDidEnterBackground(_ application: UIApplication) {
        // Use this method to release shared resources, save user data, invalidate timers, and store enough application state information to restore your application to its current state in case it is terminated later.
        // If your application supports background execution, this method is called instead of applicationWillTerminate: when the user quits.
    }

    func applicationWillEnterForeground(_ application: UIApplication) {
        // Called as part of the transition from the background to the active state; here you can undo many of the changes made on entering the background.
    }

    func applicationDidBecomeActive(_ application: UIApplication) {
        // Restart any tasks that were paused (or not yet started) while the application was inactive. If the application was previously in the background, optionally refresh the user interface.
    }

    func applicationWillTerminate(_ application: UIApplication) {
        // Called when the application is about to terminate. Save data if appropriate. See also applicationDidEnterBackground:.
    }


}

