//
//  SettingsViewController.swift
//  Imbanak
//
//  Created by Apple on 7/14/19.
//  Copyright © 2019 alfaSolutions. All rights reserved.
//

import UIKit

class SettingsViewController: UIViewController {
    let langStr = Locale.current.languageCode
    
    override func viewDidLoad() {
        super.viewDidLoad()
        amlang.setOn(false, animated: true)
        rulang.setOn(false, animated: true)
        enlang.setOn(false, animated: true)
        
        switch langStr {
        case "hy":
            amlang.setOn(true, animated: true);break;
        case "ru":
            rulang.setOn(true, animated: true);break;
        case "en":
            enlang.setOn(true, animated: true);break;
        default:
            rulang.setOn(true, animated: true);
        }
        // Do any additional setup after loading the view.
    }
    
    @IBOutlet weak var rulang: UISwitch!
    
    @IBOutlet weak var enlang: UISwitch!
    
    @IBOutlet weak var amlang: UISwitch!
    
   
     @IBAction func ruchange(_ sender: UISwitch) {
        if(sender.isOn==true){
            amlang.setOn(false, animated: true)
            enlang.setOn(false, animated: true)
            
        }else{
            amlang.setOn(true, animated: true)
        }
        
     }
    @IBAction func enchange(_ sender: UISwitch) {
       
        if(sender.isOn==true){
            amlang.setOn(false, animated: true)
            rulang.setOn(false, animated: true)
        }else{
            amlang.setOn(true, animated: true)
        }
        
    }
    @IBAction func amchange(_ sender: UISwitch) {
        
        if(sender.isOn==true){
            enlang.setOn(false, animated: true)
            rulang.setOn(false, animated: true)
        }else{
            amlang.setOn(true, animated: true)
        }
        
        
        
    }
    /*
     // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}
