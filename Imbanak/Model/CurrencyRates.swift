//
//  CurrencyRates.swift
//  Imbanak
//
//  Created by Hovak Davtyan on 1/26/18.
//  Copyright © 2018 alfaSolutions. All rights reserved.
//

import Foundation

struct CurrencyRates {
    let status: String
    struct Data {
        let usd: Double?
        let eur: Double?
        let rub: Double?
        init(usd: Double?, eur: Double?, rub: Double?) {
            self.usd = usd
            self.eur = eur
            self.rub = rub
        }
        init?(json: [String: Any]) {
            let usd = json["usd"] as? Double
            let eur = json["eur"] as? Double
            let rub = json["rub"] as? Double
            self.init(usd: usd, eur: eur, rub: rub)
        }
    }
    let data: Data
    init(status: String, data: Data) {
        self.status = status
        self.data = data
    }
    init?(json: [String: Any]) {
        guard let status = json["status"] as? String else { return nil }
        guard let dataJSONDictionary = json["data"] as? [String: Any] else { return nil }
        guard let data = Data(json: dataJSONDictionary) else { return nil }
        self.init(status: status, data: data)
    }
}
