//
//  Location.swift
//  Imbanak
//
//  Created by Hovak Davtyan on 1/9/18.
//  Copyright © 2018 alfaSolutions. All rights reserved.
//

import Foundation
import GoogleMaps

struct Location: Codable, Equatable {
    static func ==(lhs: Location, rhs: Location) -> Bool {
        return lhs.title == rhs.title
    }
    
    var thumb: String
    var type: LocationType
    var title: String
    var longitude: Double
    var latitude: Double
    var address: String
    var phoneTitles: [String]
    var phoneNumbers: [String]
}

enum LocationType: String, Codable {
    case general
    case milOffice
    case university
    case hospital
}
