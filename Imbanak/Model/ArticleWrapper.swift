//
//  NewsArticle.swift
//  Imbanak
//
//  Created by Hovak Davtyan on 12/18/17.
//  Copyright © 2017 alfaSolutions. All rights reserved.
//

import Foundation

struct ArticleWrapper {
    let status: String
    let article: Article
    
    init(status: String, article: Article) {
        self.status = status
        self.article = article
    }
    init?(json: [String: Any]) {
        guard let status = json["status"] as? String else { return nil }
        guard let articleJSONDictionary = json["article"] as? [String: Any] else { return nil }
        guard let article = Article(json: articleJSONDictionary) else { return nil }
        self.init(status: status, article: article)
    }
}

struct Article {
    var fullURL: URL? {
        return URL(string: "http://modpub.ticcotaa.ru/hy/news/" + "\(id)")
    }
    let id: Int
    let title: String?
    let description: String?
    let image: URL
    struct Topic {
        let id: Int
        let title: String?
        let description: String?
        init(id: Int, title: String?, description: String?) {
            self.id = id
            self.title = title
            self.description = description
        }
        init?(json: [String: Any]) {
            guard let id = json["id"] as? Int else { return nil }
            let title = json["title"] as? String
            let description = json["description"] as? String
            self.init(id: id, title: title, description: description)
        }
    }
    let topic: Topic?
    struct Maintag {
        let id: Int
        let title: String?
        init(id: Int, title: String?) {
            self.id = id
            self.title = title
        }
        init?(json: [String: Any]?) {
            guard let json = json else { return nil }
            guard let id = json["id"] as? Int else { return nil }
            let title = json["title"] as? String
            self.init(id: id, title: title)
        }
    }
    let maintag: Maintag?
    let publishedAt: Date
    enum Thumb: String {
        case generic
        case gallery
        case video
    }
    let thumb: Thumb
    enum Source: String {
        case mil
        case hayzinvor
        case mandatory
        case contract
        case tactical
        case staff
        case service
        case product
    }
    let source: Source
    struct Block {
        let title: String?
        let subtitle: String?
        enum BlockType: String {
            case text
            case medias
        }
        let blockType: BlockType
        let content: String?
        struct Media {
            let id: Int
            let title: String?
            let description: String?
            enum MediaType: String {
                case image
                case video
            }
            let mediaType: MediaType
            let image: URL?
            let video: URL?
            init(id: Int, title: String?, description: String?, mediaType: MediaType, image: URL?, video: URL?) {
                self.id = id
                self.title = title ?? ""
                self.description = description ?? ""
                self.mediaType = mediaType
                self.image = image
                self.video = video
            }
            init?(json: [String: Any]) {
                guard let id = json["id"] as? Int else { return nil }
                let title = json["title"] as? String
                let description = json["description"] as? String
                guard let mediaTypeRawValue = json["type"] as? String else { return nil }
                guard let mediaType = MediaType(rawValue: mediaTypeRawValue) else { return nil }
                var image: URL?
                if let imageString = json["image"] as? String {
                    image = URL(string: imageString)
                }
                var video: URL?
                if let videoString = json["video"] as? String {
                    video = URL(string: videoString)
                }
                self.init(id: id, title: title, description: description, mediaType: mediaType, image: image, video: video)
            }
        }
        let media: [Media]
        init(title: String?, subtitle: String?, blockType: BlockType, content: String?, media: [Media]) {
            self.title = title
            self.subtitle = subtitle
            self.blockType = blockType
            self.content = content
            self.media = media
        }
        init?(json: [String: Any]) {
            let title = json["title"] as? String
            let subtitle = json["subtitle"] as? String
            guard let blockTypeRawValue = json["type"] as? String else { return nil }
            guard let blockType = BlockType(rawValue: blockTypeRawValue) else { return nil }
            let content = json["content"] as? String
            guard let mediaJSONArray = json["media"] as? [[String: Any]] else { return nil }
            let media = mediaJSONArray.map({ Media(json: $0) }).compactMap({ $0 })
            self.init(title: title, subtitle: subtitle, blockType: blockType, content: content, media: media)
        }
    }
    let blocks: [Block]
    let relatedArticles: [Headline]
    
    init(id: Int, title: String?, description: String?, image: URL, topic: Topic?, maintag: Maintag?, publishedAt: Date, thumb: Thumb, source: Source, blocks: [Block], relatedArticles: [Headline]) {
        self.id = id
        self.title = title
        self.description = description
        self.image = image
        self.topic = topic
        self.maintag = maintag
        self.publishedAt = publishedAt
        self.thumb = thumb
        self.source = source
        self.blocks = blocks
        self.relatedArticles = relatedArticles
    }
    init?(json: [String: Any]) {
        guard let id = json["id"] as? Int else { return nil }
        print(id)
        let title = json["title"] as? String
        let description = json["description"] as? String
        guard let imageString = json["image"] as? String else { return nil }
        guard let image = URL(string: imageString) else { return nil }
        let topic = json["topic"] as? Topic
        let maintagJSONDictionary = json["maintag"] as? [String: Any]
        let maintag = Maintag(json: maintagJSONDictionary)
        guard let publishedAtTimeInterval = json["published_at"] as? TimeInterval else { return nil }
        let publishedAt = Date(timeIntervalSince1970: publishedAtTimeInterval)
        guard let thumbRawValue = json["thumb"] as? String else { return nil }
        guard let thumb = Thumb(rawValue: thumbRawValue) else { return nil }
        guard let sourceRawValue = json["source"] as? String else { return nil }
        guard let source = Source(rawValue: sourceRawValue) else { return nil }
        guard let blocksJSONArray = json["blocks"] as? [[String: Any]] else { return nil }
        let blocks = blocksJSONArray.map({ Block(json: $0) }).compactMap({ $0 })
        guard let relatedArticlesJSONArray = json["related"] as? [[String: Any]] else { return nil }
        let relatedArticles = relatedArticlesJSONArray.map({ Headline(json: $0) }).compactMap({ $0 })
        self.init(id: id, title: title, description: description, image: image, topic: topic, maintag: maintag, publishedAt: publishedAt, thumb: thumb, source: source, blocks: blocks, relatedArticles: relatedArticles)
    }
}
