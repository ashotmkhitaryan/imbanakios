//
//  NetworkController.swift
//  Imbanak
//
//  Created by Hovak Davtyan on 3/28/18.
//  Copyright © 2018 alfaSolutions. All rights reserved.
//

import Foundation

class NetworkController {
    private let baseURL = "http://www.mil.am/api/v1/articles/" //Should be conditional
    var currentTask: URLSessionTask?
    
    func loadHeadlines(from: Int, to: Int, ofType type: RequestType, completion: @escaping ([Headline]?) -> Void) {
        let urlString = baseURL + type.rawValue + "/" + "?start=\(from)&limit=\(to)"
        guard let url = URL(string: urlString) else {
            completion(nil)
            return
        }
        
        let configuration = URLSessionConfiguration.ephemeral
        configuration.waitsForConnectivity = true
        
        let session = URLSession(configuration: configuration, delegate: nil, delegateQueue: OperationQueue.main)
        currentTask = session.dataTask(with: url) { (data: Data?, response: URLResponse?, error: Error?) -> Void in
            
            guard
                let data = data,
                let jsonRaw = try? JSONSerialization.jsonObject(with: data, options: .mutableContainers),
                let json = jsonRaw as? [String: Any],
                let response = HeadlineWrapper(json: json)
            else {
                completion(nil)
                return
            }
            
            if response.status == "OK" {
                completion(response.headlines)
            }
            
            session.finishTasksAndInvalidate()
        }
        currentTask?.resume()
    }
    
    func loadArticle(byID id:Int, completion: @escaping(Article?) -> Void) {
        let urlString = baseURL + "\(id)"
        guard let articleURL = URL(string: urlString) else {
            completion(nil)
            return
        }
        
        let configuration = URLSessionConfiguration.ephemeral
        configuration.waitsForConnectivity = true
        
        let session = URLSession(configuration: configuration, delegate: nil, delegateQueue: OperationQueue.main)
        currentTask = session.dataTask(with: articleURL) { (data: Data?, response: URLResponse?, error: Error?) -> Void in
            
            guard
                let data = data,
                let jsonRaw = try? JSONSerialization.jsonObject(with: data, options: .mutableContainers),
                let json = jsonRaw as? [String: Any],
                let response = ArticleWrapper(json: json)
                else {
                    completion(nil)
                    return
            }
            
            if response.status == "OK" {
                completion(response.article)
            }
            
            session.finishTasksAndInvalidate()
        }
        currentTask?.resume()
    }
    func search(_ key: String, completion: @escaping ([Headline]?) -> Void) {
        
        let urlString = baseURL + "?q=" + "\(key)" + "&limit=50"
        
        guard let encodedURLString = urlString.addingPercentEncoding(withAllowedCharacters: .urlFragmentAllowed),
            let url = URL(string: encodedURLString) else {
                completion(nil)
                return
        }
        
        
        
        let configuration = URLSessionConfiguration.ephemeral
        configuration.waitsForConnectivity = true
        
        let session = URLSession(configuration: configuration, delegate: nil, delegateQueue: OperationQueue.main)
        currentTask = session.dataTask(with: url) { (data: Data?, response: URLResponse?, error: Error?) -> Void in
            
            guard
                let data = data,
                let jsonRaw = try? JSONSerialization.jsonObject(with: data, options: .mutableContainers),
                let json = jsonRaw as? [String: Any],
                let response = HeadlineWrapper(json: json)
                else {
                    completion(nil)
                    return
            }
            
            if response.status == "OK" {
                completion(response.headlines)
            }
            
            session.finishTasksAndInvalidate()
        }
        currentTask?.resume()
    }
}

enum RequestType: String {
    case newsfeed = "newsfeed"
    case milServices = "milservices"
    case announcements = "announcements"
}
