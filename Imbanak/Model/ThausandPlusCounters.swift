//
//  ThausandPlusCounters.swift
//  Imbanak
//
//  Created by Hovak Davtyan on 1/26/18.
//  Copyright © 2018 alfaSolutions. All rights reserved.
//

import Foundation

struct ThausandPlusCounters {
    let status: String
    struct Data {
        let mandatorySum: Double
        let freewillSum: Double
        let profitSum: Double
        let compensationSum: Double
        init(mandatorySum: Double, freewillSum: Double, profitSum: Double, compensationSum: Double) {
            self.mandatorySum = mandatorySum
            self.freewillSum = freewillSum
            self.profitSum = profitSum
            self.compensationSum = compensationSum
        }
        init?(json: [String: Any]) {
            guard let mandatorySum = json["mandatory_sum"] as? Double else { return nil }
            guard let freewillSum = json["freewill_sum"] as? Double else { return nil }
            guard let profitSum = json["profit_sum"] as? Double else { return nil }
            guard let compensationSum = json["compensation_sum"] as? Double else { return nil }
            self.init(mandatorySum: mandatorySum, freewillSum: freewillSum, profitSum: profitSum, compensationSum: compensationSum)
        }
    }
    let data: Data
    init(status: String, data: Data) {
        self.status = status
        self.data = data
    }
    init?(json: [String: Any]) {
        guard let status = json["status"] as? String else { return nil }
        guard let dataJSONDictionary = json["data"] as? [String: Any] else { return nil }
        guard let data = Data(json: dataJSONDictionary) else { return nil }
        self.init(status: status, data: data)
    }
}
