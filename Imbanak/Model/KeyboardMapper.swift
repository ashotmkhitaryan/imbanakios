//
//  KeyboardMapper.swift
//  Imbanak
//
//  Created by Hovak Davtyan on 1/10/18.
//  Copyright © 2018 alfaSolutions. All rights reserved.
//

import Foundation

struct KeyboardMapper {
    
    static let pairs = [
        "zh" : "ժ",
        "dz" : "ձ",
        "gh" : "ղ",
        "sh" : "շ",
        "ev" : "և",
        "ts" : "ց",
        "kh" : "խ",
        "ch" : "չ"
    ]
    
    static let singleKeys = [
        "a" : "ա",
        "b" : "բ",
        "g" : "գ",
        "d" : "դ",
        "e" : "ե",
        "z" : "զ",
        "@" : "ը",
        "i" : "ի",
        "l" : "լ",
        "k" : "կ",
        "h" : "հ",
        "x" : "ղ",
        "m" : "մ",
        "j" : "ջ",
        "n" : "ն",
        "o" : "ո",
        "p" : "պ",
        "s" : "ս",
        "v" : "վ",
        "t" : "տ",
        "r" : "ր",
        "c" : "ց",
        "u" : "ու",
        "q" : "ք",
        "f" : "ֆ",
        "w" : "վ",
        "y" : "ը"
    ]
    
    static let armenianDiffs = [
        //Singles
        "ե" : "է",
        "զ" : "ձ",
        "կ" : "ք",
        "ղ" : "խ",
        "ջ" : "յ",
        "ո" : "օ",
        "պ" : "փ",
        "տ" : "թ",
        "ր" : "ռ",
        "ց" : "ծ",
        "ը" : "յ",
        
        //Pairs
        "խ" : "ղ",
        "չ" : "ճ"
        
    ]
    
    static func map(engText: String) -> [String] {
        let versions = detectPairs(text: engText.lowercased())
        var results = Set<String>()
        
        for version in versions {
            
            //First Round
            var armtext = ""
            for character in version {
                
                if let key = singleKeys["\(character)"] {
                    armtext.append(key)
                } else {
                    armtext.append(character)
                }
            }
            results.insert(armtext)
            
            //Second Round
            for index  in armtext.indices {
                let character = armtext[index]
                
                if let key = armenianDiffs["\(character)"] {
                    var modifiedArmText = armtext
                    modifiedArmText.remove(at: index)
                    modifiedArmText.insert(Character(key), at: index)
                    results.insert(modifiedArmText)
                }
            }
        }
        return Array(results)
    }
    
    static func detectPairs(text: String) -> [String] {
        
        var results = Set<String>()
        results.insert(text)
        
        var textToAlter = text
        
        for key in pairs.keys {
            while let range = textToAlter.range(of: key) {
                let alteredText = textToAlter.replacingCharacters(in: range, with: pairs[key] ?? "")
                results.insert(alteredText)
                results.insert(textToAlter)
                textToAlter = alteredText
            }
        }
        
        return Array(results)
    }
}
