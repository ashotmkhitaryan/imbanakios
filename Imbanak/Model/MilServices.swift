//
//  MilServices.swift
//  Imbanak
//
//  Created by Hovak Davtyan on 1/15/18.
//  Copyright © 2018 alfaSolutions. All rights reserved.
//

import Foundation

struct MilServices {
    let status: String
    let total: Int
    let count: Int
    struct Article {
        let id: Int
        let title: String?
        let topicId: Int?
        let publishedAt: Date
        enum Thumb: String {
            case gallery
            case video
            case generic
        }
        let thumb: Thumb
        enum Source: String {
            case mandatory
            case contract
            case tactical
        }
        let source: Source
        let featuredImage: URL
        struct Maintag {
            let id: Int
            let title: String
            init(id: Int, title: String) {
                self.id = id
                self.title = title
            }
            init?(json: [String: Any]?) {
                guard let json = json else { return nil }
                guard let id = json["id"] as? Int else { return nil }
                guard let title = json["title"] as? String else { return nil }
                self.init(id: id, title: title)
            }
        }
        let maintag: Maintag?
        struct Tag {
            let id: Int
            let title: String
            init(id: Int, title: String) {
                self.id = id
                self.title = title
            }
            init?(json: [String: Any]) {
                guard let id = json["id"] as? Int else { return nil }
                guard let title = json["title"] as? String else { return nil }
                self.init(id: id, title: title)
            }
        }
        let tags: [Tag]
        init(id: Int, title: String?, topicId: Int?, publishedAt: Date, thumb: Thumb, source: Source, featuredImage: URL, maintag: Maintag?, tags: [Tag]) {
            self.id = id
            self.title = title
            self.topicId = topicId
            self.publishedAt = publishedAt
            self.thumb = thumb
            self.source = source
            self.featuredImage = featuredImage
            self.maintag = maintag
            self.tags = tags
        }
        init?(json: [String: Any]) {
            guard let id = json["id"] as? Int else { return nil }
            let title = json["title"] as? String
            let topicId = json["topic_id"] as? Int
            guard let publishedAtTimeInterval = json["published_at"] as? TimeInterval else { return nil }
            let publishedAt = Date(timeIntervalSince1970: publishedAtTimeInterval)
            guard let thumbRawValue = json["thumb"] as? String else { return nil }
            guard let thumb = Thumb(rawValue: thumbRawValue) else { return nil }
            guard let sourceRawValue = json["source"] as? String else { return nil }
            guard let source = Source(rawValue: sourceRawValue) else { return nil }
            guard let featuredImageString = json["featured_image"] as? String else { return nil }
            guard let featuredImage = URL(string: featuredImageString) else { return nil }
            let maintagJSONDictionary = json["maintag"] as? [String: Any]
            let maintag = Maintag(json: maintagJSONDictionary)
            guard let tagsJSONArray = json["tags"] as? [[String: Any]] else { return nil }
            let tags = tagsJSONArray.map({ Tag(json: $0) }).flatMap({ $0 })
            self.init(id: id, title: title, topicId: topicId, publishedAt: publishedAt, thumb: thumb, source: source, featuredImage: featuredImage, maintag: maintag, tags: tags)
        }
    }
    let articles: [Article]
    init(status: String, total: Int, count: Int, articles: [Article]) {
        self.status = status
        self.total = total
        self.count = count
        self.articles = articles
    }
    init?(json: [String: Any]) {
        guard let status = json["status"] as? String else { return nil }
        guard let total = json["total"] as? Int else { return nil }
        guard let count = json["count"] as? Int else { return nil }
        guard let articlesJSONArray = json["articles"] as? [[String: Any]] else { return nil }
        let articles = articlesJSONArray.map({ Article(json: $0) }).flatMap({ $0 })
        self.init(status: status, total: total, count: count, articles: articles)
    }
}
