//
//  NewsNavigationController.swift
//  Imbanak
//
//  Created by Hovak Davtyan on 12/23/17.
//  Copyright © 2017 alfaSolutions. All rights reserved.
//

import UIKit

class IBNavigationController: UINavigationController {
    
    var isShowingLargeTitle: Bool!
    
    override var shouldAutorotate: Bool {
        return (visibleViewController?.shouldAutorotate)!
    }
    
    override var supportedInterfaceOrientations: UIInterfaceOrientationMask {
        return (visibleViewController?.supportedInterfaceOrientations)!
    }
    
    override var preferredInterfaceOrientationForPresentation: UIInterfaceOrientation {
        return .portrait
    }
    
    func switchStyle () {
        if navigationBar.frame.height <= CGFloat(45) {
            isShowingLargeTitle = false
            
            print(navigationBar.frame.height)
            
            navigationBar.barTintColor = UIColor.piWhite
            navigationBar.setValue(false, forKey: "hidesShadow")
            
            //Configure Search
            visibleViewController?.navigationItem.searchController?.searchBar.setSearchFieldBackground(color: UIColor.piSteel.withAlphaComponent(0.12),
                                                                                                       height: 36, cornerRadius: 6,
                                                                                                       for: .normal)
        } else if navigationBar.frame.height > CGFloat(45) {
            isShowingLargeTitle = true
            
            print(navigationBar.frame.height)
            
            navigationBar.barTintColor = UIColor.piWhiteTwo
            navigationBar.setValue(true, forKey: "hidesShadow")
            
            //Configure Search
            visibleViewController?.navigationItem.searchController?.searchBar.setSearchFieldBackground(color: UIColor.piWhiteThree,
                                                                                                       height: 48, cornerRadius: 6,
                                                                                                       for: .normal)
        }
    }
}
