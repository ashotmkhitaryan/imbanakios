//
//  Extensions.swift
//  Imbanak
//
//  Created by Hovak Davtyan on 12/23/17.
//  Copyright © 2017 alfaSolutions. All rights reserved.
//

import Foundation
import UIKit

// MARK: - UIImage Extension

extension UISearchBar {
    func setSearchFieldBackground(color: UIColor, height: CGFloat, cornerRadius: Int, for state: UIControlState) {
        
        let searchFieldBackgroundImage = UIImage.imageWith(color: color,
                                                           size: CGSize(width: 100, height: height))
        
        let searchFieldBackgroundImageWithCornerRadius = UIImage.roundedImage(image: searchFieldBackgroundImage,
                                                                              cornerRadius: cornerRadius)
        
        self.setSearchFieldBackgroundImage(searchFieldBackgroundImageWithCornerRadius, for: state)
        self.searchTextPositionAdjustment = UIOffsetMake(8.0, 0.0)
    }
}

extension UIImage {
    class func imageWith(color: UIColor, size: CGSize) -> UIImage {
        let rect: CGRect = CGRect(x: 0, y: 0, width: size.width, height: size.height)
        UIGraphicsBeginImageContextWithOptions(size, false, 0)
        color.setFill()
        UIRectFill(rect)
        let image: UIImage = UIGraphicsGetImageFromCurrentImageContext()!
        UIGraphicsEndImageContext()
        return image
    }
    
    class func roundedImage(image: UIImage, cornerRadius: Int) -> UIImage {
        let rect = CGRect(origin:CGPoint(x: 0, y: 0), size: image.size)
        UIGraphicsBeginImageContextWithOptions(image.size, false, image.scale)
        UIBezierPath(
            roundedRect: rect,
            cornerRadius: CGFloat(cornerRadius)
            ).addClip()
        image.draw(in: rect)
        return UIGraphicsGetImageFromCurrentImageContext()!
    }
}

extension UIView {
    func startShimmering(withIntensity intensity: CGFloat) {
        let light = UIColor.init(white: 0, alpha: intensity).cgColor
        let dark = UIColor.black.cgColor
        
        let gradient: CAGradientLayer = CAGradientLayer()
        if intensity > 0.7 {
            gradient.colors = [light, dark, light]
        } else {
            gradient.colors = [dark, light, dark]
        }
        gradient.frame = CGRect(x: -self.bounds.size.width, y: 0, width: 3 * self.bounds.size.width, height: self.bounds.size.height)
        gradient.startPoint = CGPoint(x: 0.0, y: 0.5) //Angle
        gradient.endPoint = CGPoint(x: 1.0, y: 0.525) //Angle
        gradient.locations = [0.5, 0.6, 0.7]
        self.layer.mask = gradient
        
        let animation: CABasicAnimation = CABasicAnimation(keyPath: "locations")
        animation.fromValue = [0.0, 0.1, 0.2]
        animation.toValue = [0.8, 0.9, 1.0]
        
        animation.duration = 1.5
        animation.repeatCount = HUGE
        gradient.add(animation, forKey: "shimmer")
    }
    
    func stopShimmering() {
        self.layer.mask = nil
    }
}

extension Date {
    var stringValue: String {
        //Date
        let dateFormatter = DateFormatter()
        dateFormatter.dateStyle = .long
        dateFormatter.locale = Locale(identifier: "hy")
        
        let calendar = Calendar.current
        
        //Current Date components
        let current = Date()
        let currentDay = calendar.component(.day, from: current)
        
        //Article date components
        let articleDay = calendar.component(.day, from: self)
        if articleDay == currentDay {
            return "Այսօր"
        } else if articleDay == currentDay - 1 {
            return "Երեկ"
        } else {
            return dateFormatter.string(from: self)
        }
    }
}
