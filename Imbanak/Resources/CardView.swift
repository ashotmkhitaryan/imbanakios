//
//  CardView.swift
//  Imbanak
//
//  Created by Hovak Davtyan on 3/4/18.
//  Copyright © 2018 alfaSolutions. All rights reserved.
//

import UIKit

class CardView: UIView {
    //private var faceView: UIView!
    private var shadow1View: UIView!
    private var shadow2View: UIView!
    
    @IBInspectable var cornerRadius: CGFloat = 5.0
    @IBInspectable var shadowColor: UIColor = .black
    @IBInspectable var shadowIntensity: Int = 1
    
    required init?(coder: NSCoder) {
        super.init(coder: coder)
        
        shadow1View = UIView()
        addSubview(shadow1View)
        sendSubview(toBack: shadow1View)
        
        shadow2View = UIView()
        addSubview(shadow2View)
        sendSubview(toBack: shadow2View)
    }
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        
        shadow2View = UIView()
        addSubview(shadow2View)
        sendSubview(toBack: shadow2View)
        
        shadow1View = UIView()
        addSubview(shadow1View)
        sendSubview(toBack: shadow1View)
    }
    
    override func layoutSubviews() {
        super.layoutSubviews()
        
        shadow1View.frame = bounds
        shadow1View.layer.shadowColor = shadowColor.cgColor
        shadow1View.layer.shadowOpacity = 0.16 * Float(shadowIntensity)
        shadow1View.layer.shadowOffset = CGSize.zero
        shadow1View.layer.shadowRadius = 0.5
        shadow1View.layer.shadowPath = UIBezierPath(roundedRect: bounds, cornerRadius: self.cornerRadius).cgPath

        shadow2View.frame = bounds
        shadow2View.layer.shadowColor = shadowColor.cgColor
        shadow2View.layer.shadowOpacity = 0.24 * Float(shadowIntensity)
        shadow2View.layer.shadowOffset = CGSize(width: 0.0, height: 0.5)
        shadow2View.layer.shadowRadius = 0.5
        shadow2View.layer.shadowPath = UIBezierPath(roundedRect: bounds, cornerRadius: self.cornerRadius).cgPath
    }
}
