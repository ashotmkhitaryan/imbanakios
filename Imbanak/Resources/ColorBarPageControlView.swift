//
//  ColorBarPageControlView.swift
//  Imbanak
//
//  Created by Hovak Davtyan on 12/21/17.
//  Copyright © 2017 alfaSolutions. All rights reserved.
//

import UIKit

class ColorBarPageControlView: UIView {
    private var preIndicatorView: UIView!
    private var indicatorView: UIView!
    private var unitWidth: CGFloat = 1.0
    public var numberOfElements: Int!
    public var color: UIColor!
    
    public func adjustIndicator(position: Int) {
        
        let preIndicatorWidth = unitWidth * CGFloat(position)
        self.preIndicatorView.frame.size.width = preIndicatorWidth
        
        let indicatorViewXPosition = CGFloat(position) * unitWidth
        self.indicatorView.frame.origin.x = indicatorViewXPosition
        
        self.setNeedsLayout()
        self.layoutIfNeeded()
        
    }
    
    override func layoutSubviews() {
        super.layoutSubviews()
        
        self.unitWidth = self.frame.width / CGFloat(numberOfElements)
        self.indicatorView.frame.size.width = unitWidth
        
    }
    
    init(frame: CGRect, startIndex: Int, color: UIColor, numberOfElements: Int) {
        
        super.init(frame: CGRect(x: 0, y: 0, width: 0, height: 0))
        self.frame = CGRect(x: 0, y: 0, width: frame.width, height: 8)
        
        self.numberOfElements = numberOfElements
        
        if self.numberOfElements == 1 {
            self.numberOfElements = 6
        }
        
        self.unitWidth = self.frame.width / CGFloat(numberOfElements)
        
        self.preIndicatorView = UIView(frame: CGRect(x: 0, y: 0, width: 0, height: 8))
        self.indicatorView = UIView(frame: CGRect(x: 0, y: 0, width: unitWidth, height: 8))
        
        self.backgroundColor = color.withAlphaComponent(0.75)
        self.preIndicatorView.backgroundColor = color.withAlphaComponent(0.6)
        self.indicatorView.backgroundColor = color
        
        self.addSubview(preIndicatorView)
        self.addSubview(indicatorView)
        
        adjustIndicator(position: startIndex)
    }
    
    required init?(coder: NSCoder) {
        super.init(coder: coder)
    }
}
