//
//  IBTabBarController.swift
//  Imbanak
//
//  Created by Hovak Davtyan on 11/29/17.
//  Copyright © 2017 alfaSolutions. All rights reserved.
//

import UIKit

class IBTabBarController: UITabBarController {

    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.tabBar.tintColor = UIColor.piYellowOrange
        self.tabBar.unselectedItemTintColor = UIColor.piDarkBlueGrey
        self.tabBar.barTintColor = UIColor.piWhite

        // Do any additional setup after loading the view.
    }
    
    override var shouldAutorotate: Bool {
        return (selectedViewController?.shouldAutorotate)!
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    override func prefersHomeIndicatorAutoHidden() -> Bool
    {
        print("IndicatorState")
        return true
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
