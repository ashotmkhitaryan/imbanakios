//
//  SearchSectionsHeaderView.swift
//  Imbanak
//
//  Created by Hovak Davtyan on 1/25/18.
//  Copyright © 2018 alfaSolutions. All rights reserved.
//

import UIKit

class SearchSectionsHeaderView: UITableViewCell {
    
    @IBOutlet weak var sectionTitleLabel: UILabel!
    @IBOutlet weak var actionButton: UIButton!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
}
