//
//  SearchTableViewController.swift
//  Imbanak
//
//  Created by Hovak Davtyan on 12/11/17.
//  Copyright © 2017 alfaSolutions. All rights reserved.
//

import UIKit

class SearchTableViewController: UITableViewController, UISearchControllerDelegate, UISearchResultsUpdating {
    
    var networkController: NetworkController!
    private lazy var scaledFont = ScaledFont.init(fontName: "WeeblySleekUI")
    let defaults = UserDefaults.standard
    
    var hasSearched = false
    
    var news = [Headline]()
    var newsLimit = 50
    
    var searchHistory = [Headline]()
    
    func updateSearchResults(for searchController: UISearchController) {
        guard let searchEntry = searchController.searchBar.text else { return }
        
        networkController.currentTask?.cancel()
        
        if searchEntry == "" {
            hasSearched = false
            news = [Headline]()
            tableView.reloadData()
            view.isHidden = false
        } else if searchEntry.count >= 3 {
            hasSearched = true
            networkController.search(searchEntry) { [weak self]
                headlines in
                guard let headlines = headlines else { return }
                self?.news = headlines.filter { $0.source == .mil || $0.source == .hayzinvor }
                self?.tableView.reloadData()
            }
        }
        
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        //Get history
        if let historyData = defaults.data(forKey: "searchHistory") {
            if let decodedData = try? JSONDecoder().decode([Headline].self, from: historyData) {
                searchHistory = decodedData
            }
        } else {
            //Save to userDefaults
            let encodedData = try? JSONEncoder().encode(searchHistory)
            defaults.set(encodedData, forKey: "searchHistory")
            defaults.synchronize()
        }
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    // MARK: - Table view data source
    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if !hasSearched {
            return searchHistory.count
        } else {
            if news.count > newsLimit {
                return newsLimit
            } else {
                return news.count
            }
        }
    }
    
    
    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "searchCell", for: indexPath)

        cell.textLabel?.font = scaledFont.font(forTextStyle: .headline)
        
        if !hasSearched {
            cell.textLabel?.text = searchHistory[indexPath.row].title
        } else {
            cell.textLabel?.text = news[indexPath.row].title
        }
        return cell
    }
    
    override func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        //Prepare for Navigation
        let tabbar = UIApplication.shared.keyWindow?.rootViewController as? IBTabBarController
        let navigationController = tabbar?.selectedViewController as? UINavigationController
        var headline: Headline?
        if !hasSearched {
            headline = searchHistory[indexPath.row]
        } else {
            headline = news[indexPath.row]
        }
        
        if let headline = headline {
            //Adding to History
            if hasSearched {
                if searchHistory.contains(headline) {
                    if let index = searchHistory.index(of: headline) {
                        searchHistory.remove(at: index)
                    }
                }
                
                searchHistory.insert(headline, at: 0)
                
                let encodedData = try? JSONEncoder().encode(searchHistory)
                defaults.set(encodedData, forKey: "searchHistory")
                defaults.synchronize()
            }
            
            let visibleVC = navigationController?.visibleViewController
            
            switch headline.thumb {
            case .generic: visibleVC?.performSegue(withIdentifier: "SearchNewsSegue", sender: headline)
            case .video: visibleVC?.performSegue(withIdentifier: "SearchVideoSegue", sender: headline)
            default: break
            }
        }
    }
    
    override func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        let sectionHeaderView  = Bundle.main.loadNibNamed("SearchSectionsHeaderView", owner: self, options: nil)?.first as! SearchSectionsHeaderView
        
        if !hasSearched {
            sectionHeaderView.sectionTitleLabel.font = scaledFont.font(forTextStyle: .footnote)
            sectionHeaderView.sectionTitleLabel.text = "Որոնման Պատմություն"
            sectionHeaderView.actionButton.titleLabel?.font = scaledFont.font(forTextStyle: .footnote)
            sectionHeaderView.actionButton.setTitle("Մաքրել", for: .normal)
            if searchHistory.count == 0 {
                sectionHeaderView.actionButton.isEnabled = false
            }
        }
        
        sectionHeaderView.actionButton.addTarget(self, action: #selector(actionButtonTapped(_:)), for: .touchUpInside)
        return sectionHeaderView
    }
    
    override func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        let aspect = scaledFont.font(forTextStyle: .footnote).pointSize / 13
        return hasSearched ? 0 : 45 * aspect
    }
    
    override func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        let aspect = scaledFont.font(forTextStyle: .headline).pointSize / 17
        return 65 * aspect
    }
    
    @objc func actionButtonTapped(_ sender: UIButton) {
            //Clear Search History and synchronize
            searchHistory = [Headline]()
            let encodedData = try? JSONEncoder().encode(searchHistory)
            defaults.set(encodedData, forKey: "searchHistory")
            defaults.synchronize()
            tableView.reloadData()
    }
}
