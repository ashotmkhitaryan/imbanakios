//
//  ColorBar.swift
//  Imbanak
//
//  Created by Hovak Davtyan on 3/3/18.
//  Copyright © 2018 alfaSolutions. All rights reserved.
//

import UIKit

class ColorBar: UIView {
    
    //Set Color
    var color: UIColor! {
        didSet {
            colorBarViews[0].backgroundColor = color
            colorBarViews[1].backgroundColor = color.withAlphaComponent(0.75)
            colorBarViews[2].backgroundColor = color.withAlphaComponent(0.60)
        }
    }
    
    var id: Int!
    
    enum BarDirection {
        case horizontal
        case vertical
    }
    
    private var direction: BarDirection {
        if frame.width > frame.height {
            return .vertical
        } else {
            return .horizontal
        }
    }
    
    private var permutation:[Int]! {
        guard let id = id else {return [0,0,0]}
        let permutations = [
            0 : [0,1,2],
            1 : [0,2,1],
            2 : [1,0,2],
            3 : [1,2,0],
            4 : [2,0,1],
            5 : [2,1,0]
        ]
        
        return permutations[id % permutations.count]
    }
    
    private var colorBarViews = [UIView(), UIView(), UIView()]
    
    //Initializers
    override init(frame: CGRect) {
        super.init(frame: frame)
        
        addSubview(colorBarViews[0])
        
        addSubview(colorBarViews[1])
        
        addSubview(colorBarViews[2])
    }
    
    required init?(coder: NSCoder) {
        super.init(coder: coder)
        
        addSubview(colorBarViews[0])
        
        addSubview(colorBarViews[1])
        
        addSubview(colorBarViews[2])
    }
    
    override func layoutSubviews() {
        super.layoutSubviews()
        
        switch direction {
        case .vertical:
            //Set Lengths
            let unitLength: CGFloat = frame.width / 6
            colorBarViews[0].frame = CGRect(x: 0, y: 0, width: unitLength * 2, height: frame.height)
            colorBarViews[1].frame = CGRect(x: 0, y: 0, width: unitLength * 1, height: frame.height)
            colorBarViews[2].frame = CGRect(x: 0, y: 0, width: unitLength * 3, height: frame.height)
            
            let permutedBarViews = [colorBarViews[permutation[0]], colorBarViews[permutation[1]], colorBarViews[permutation[2]]]
            permutedBarViews[0].frame.origin.x = 0
            permutedBarViews[1].frame.origin.x = (permutedBarViews[0].frame.width)
            permutedBarViews[2].frame.origin.x = (permutedBarViews[1].frame.width) + (permutedBarViews[0].frame.width)
        case .horizontal:
            //Set Lengths
            let unitLength: CGFloat = frame.height / 6
            colorBarViews[0].frame = CGRect(x: 0, y: 0, width: frame.width, height: unitLength * 2)
            colorBarViews[1].frame = CGRect(x: 0, y: 0, width: frame.width, height: unitLength * 1)
            colorBarViews[2].frame = CGRect(x: 0, y: 0, width: frame.width, height: unitLength * 3)
            
            let permutedBarViews = [colorBarViews[permutation[0]], colorBarViews[permutation[1]], colorBarViews[permutation[2]]]
            permutedBarViews[0].frame.origin.y = 0
            permutedBarViews[1].frame.origin.y = (permutedBarViews[0].frame.height)
            permutedBarViews[2].frame.origin.y = (permutedBarViews[1].frame.height) + (permutedBarViews[0].frame.height)
        }
    }
}
