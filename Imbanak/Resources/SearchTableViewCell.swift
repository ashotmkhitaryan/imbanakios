//
//  SearchTableViewCell.swift
//  Imbanak
//
//  Created by Hovak Davtyan on 4/14/18.
//  Copyright © 2018 alfaSolutions. All rights reserved.
//

import UIKit

class SearchTableViewCell: UITableViewCell {

    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }
    
    override func prepareForReuse() {
        textLabel?.text = ""
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
