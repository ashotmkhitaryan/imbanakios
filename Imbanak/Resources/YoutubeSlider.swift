//
//  youtubeSlider.swift
//  Imbanak
//
//  Created by Hovak Davtyan on 12/25/17.
//  Copyright © 2017 alfaSolutions. All rights reserved.
//

import Foundation
import UIKit

class YoutubeSlider: UISlider {
    
        @IBInspectable var trackHeight: CGFloat = 5
    
        
        override func trackRect(forBounds bounds: CGRect) -> CGRect {
            var newRect = super.trackRect(forBounds: bounds)
            newRect.size.height = trackHeight
            return newRect
        }
    
    func showThumb() {
        let thumbImageBig = UIImage.imageWith(color: UIColor.piDeepRed, size: CGSize(width: 12, height: 12))
        let roundedImage = UIImage.roundedImage(image: thumbImageBig, cornerRadius: 12)
        self.setThumbImage(roundedImage, for: .normal)
    }
    
    func hideThumb() {
        let thumbImageBig = UIImage.imageWith(color: UIColor.clear, size: CGSize(width: 12, height: 12))
        self.setThumbImage(thumbImageBig, for: .normal)
    }
}
