//
//  IBActivityIndicator.swift
//  Imbanak
//
//  Created by Hovak Davtyan on 4/5/18.
//  Copyright © 2018 alfaSolutions. All rights reserved.
//

import UIKit

class IBActivityIndicator: UIImageView {
    private let mil = [
        UIImage(named: "loader0.pdf")!,
        UIImage(named: "loader1.pdf")!,
        UIImage(named: "loader2.pdf")!,
        UIImage(named: "loader3.pdf")!
    ]
    private let youtube = [
        UIImage(named: "loader4.pdf")!,
        UIImage(named: "loader5.pdf")!,
        UIImage(named: "loader6.pdf")!,
        UIImage(named: "loader7.pdf")!
    ]
    
    private let hayzinvor = [
        UIImage(named: "loader8.pdf")!,
        UIImage(named: "loader9.pdf")!,
        UIImage(named: "loader10.pdf")!,
        UIImage(named: "loader11.pdf")!
    ]
    
    enum IndicatorType: Int {
        case full = 0
        case mil = 1
        case youtube = 2
        case hayzinvor = 3
    }
    
    var type: IndicatorType! {
        didSet {
            switch type {
            case .mil?: self.animationImages = mil
            case .youtube?: self.animationImages = youtube
            case .hayzinvor?: self.animationImages = hayzinvor
            case .full? : self.animationImages = mil + youtube + hayzinvor
            default: break
            }
            self.animationDuration = (Double(self.animationImages?.count ?? 0) / 10.0)
        }
    }
    
    @IBInspectable var hidesWhenStopped: Bool = false
    
    init() {
        super.init(frame: CGRect.zero)
        commonInit()
    }
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        commonInit()
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        commonInit()
    }
    
    override func layoutSubviews() {
        self.layer.cornerRadius = self.frame.width / 10
    }
    
    override func stopAnimating() {
        super.stopAnimating()
        if hidesWhenStopped {
            self.isHidden = true
        }
    }
    
    func commonInit() {
        self.layer.cornerRadius = self.frame.width / 10
        self.layer.masksToBounds = true
        self.type = .full
    }
    
    func animateFor(offset: CGFloat) {
        let index = self.index(forOffset: offset)
        self.image = animationImages?[index]
    }
    
    private func index(forOffset offset: CGFloat) -> Int {
        // TODO: Row Values
        let count = self.animationImages?.count ?? 0
        let limit = Double(count) - 1
        let leftSpan = 353.0 - (195.3333)
        let rightSpan = limit - 0.0
        let scaled = (Double(abs(offset)) - (353.0)) / leftSpan
        let output = (Int(limit + (scaled * rightSpan)))
        if output >= 0 && output < count {
            return output
        } else {
            return 0
        }
    }
}
